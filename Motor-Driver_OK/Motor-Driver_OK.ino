/*
Sample Code to run the Sparkfun TB6612FNG 1A Dual Motor Driver using Arduino UNO R3

This code conducts a few simple manoeuvres to illustrate the functions:
  - motorDrive(motorNumber, motorDirection, motorSpeed)
  - motorBrake(motorNumber)
  - motorStop(motorNumber)
  - motorsStandby

Connections:
- Pin 3 ---> PWMA
- Pin 8 ---> AIN2
- Pin 9 ---> AIN1
- Pin 10 ---> STBY
- Pin 11 ---> BIN1
- Pin 12 ---> BIN2
- Pin 5 ---> PWMB

- Motor 1: A01 and A02
- Motor 2: B01 and B02

*/
#include <Encoder.h>

Encoder Enc1(2,22);//M1
Encoder Enc2(3,34);//M2
Encoder Enc3(18,17);//M3
Encoder Enc4(19,46);//M4
Encoder Enc5(20,31);//M5
Encoder Enc6(21,33);//M6

long newPosition1;
long oldPosition1  = -999;
long newPosition2;
long oldPosition2  = -999;
long newPosition3;
long oldPosition3  = -999;

//Define the Pins

//Motor 1
int M1_pinAIN1 = 26; //Direction 
int M1_pinAIN2 = 24; //Direction 
int M1_pinPWMA = 9; //Speed

//Motor 2
int M2_pinAIN1 = 30; //Direction
int M2_pinAIN2 = 32; //Direction
int M2_pinPWMA = 8; //Speed

//Motor 3
int M3_pinAIN1 = 38; //Direction 
int M3_pinAIN2 = 36; //Direction 
int M3_pinPWMA = 11; //Speed

//Motor 4
int M4_pinAIN1 = 42; //Direction 
int M4_pinAIN2 = 44; //Direction 
int M4_pinPWMA = 10; //Speed

//Motor 5
int M5_pinAIN1 = 50; //Direction 
int M5_pinAIN2 = 48; //Direction 
int M5_pinPWMA = 13; //Speed

//Motor 6
int M6_pinAIN1 = 53; //Direction 
int M6_pinAIN2 = 51; //Direction 
int M6_pinPWMA = 12; //Speed

//Standby
int pinSTBY_1 = 28;
int pinSTBY_2 = 40;
int pinSTBY_3 = 52;

//Constants to help remember the parameters
static boolean turnCW = 0;  //for motorDrive function
static boolean turnCCW = 1; //for motorDrive function
static int motor1 = 1;  //for motorDrive, motorStop, motorBrake functions
static int motor2 = 2;  //for motorDrive, motorStop, motorBrake functions
static int motor3 = 3;  //for motorDrive, motorStop, motorBrake functions
static int motor4 = 4;  //for motorDrive, motorStop, motorBrake functions
static int motor5 = 5;  //for motorDrive, motorStop, motorBrake functions
static int motor6 = 6;  //for motorDrive, motorStop, motorBrake functions

//global variables
unsigned long usPassed;
unsigned long usPassed2;
unsigned long usPassed3;
float encoderSpeed = 0;
float encoderSpeed2 = 0;
float encoderSpeed3 = 0;

//Motor variables
int voltageFeed;
int voltageFeed2;
int voltageFeed3;
const bool turnDirection = true;  //for motorDrive function
const bool turnDirection2 = true;
const bool turnDirection3 = true;


int cpr = 12;
int gearratio = 210;
int encoderValue = 2532; // cpr(12) * gearRatio(211)
int maximalVoltage = 255;

//bool M1_state = false; 
bool M1_complete = true; //判斷 M1 有沒有動
float nowPosition1 = 0; // 以圈數為單位
float nowPosition2 = 0; // 以圈數為單位
float nowPosition3 = 0; // 以圈數為單位

//M1 Rotrary encoder
int M1_PinA = 2, M1_PinB = 22;   // pushbutton connected to digital pin 7
int M1_A = 0, M1_B = 0;     // variable to store the read value
long M1_recounter = 0;
int M1_reA, M1_reB, M1_rePA, M1_rePB;
int M1_mflag = 0;

//M2 Rotrary encoder
int M2_PinA = 3, M2_PinB = 34;   // pushbutton connected to digital pin 7
int M2_A = 0, M2_B = 0;     // variable to store the read value
long M2_recounter = 0;
int M2_reA, M2_reB, M2_rePA, M2_rePB;
int M2_mflag = 0;

//M3 Rotrary encoder
int M3_PinA = 18, M3_PinB = 17;   // pushbutton connected to digital pin 7
int M3_A = 0, M3_B = 0;     // variable to store the read value
long M3_recounter = 0;
int M3_reA, M3_reB, M3_rePA, M3_rePB;
int M3_mflag = 0;

//M4 Rotrary encoder
int M4_PinA = 19, M4_PinB = 46;   // pushbutton connected to digital pin 7
int M4_A = 0, M4_B = 0;     // variable to store the read value
long M4_recounter = 0;
int M4_reA, M4_reB, M4_rePA, M4_rePB;
int M4_mflag = 0;

//M5 Rotrary encoder
int M5_PinA = 20, M5_PinB = 31;   // pushbutton connected to digital pin 7
int M5_A = 0, M5_B = 0;     // variable to store the read value
long M5_recounter = 0;
int M5_reA, M5_reB, M5_rePA, M5_rePB;
int M5_mflag = 0;

//M6 Rotrary encoder
int M6_PinA = 21, M6_PinB = 33;   // pushbutton connected to digital pin 7
int M6_A = 0, M6_B = 0;     // variable to store the read value
long M6_recounter = 0;
int M6_reA, M6_reB, M6_rePA, M6_rePB;
int M6_mflag = 0;

//轉動位置
long M1_trec = 0;
long M2_trec = 0;
long M3_trec = 0;
long M4_trec = 0;
long M5_trec = 0;
long M6_trec = 0;

//自動回正
//#define error 5
int M1_state = 0; //0 沒轉，1 轉動
int M2_state = 0;
int M3_state = 0;
int M4_state = 0;
int M5_state = 0;
int M6_state = 0;

//初始位置
long M1_init = 0;
long M2_init = 0;
long M3_init = 0;
long M4_init = 0;
long M5_init = 0;
long M6_init = 0;

//neutral pos!
long M1_neutral = 0;
long M2_neutral = 0;
long M3_neutral = 0;
long M4_neutral = 0;
long M5_neutral = 0;
long M6_neutral = 0;

//單軸、雙軸模式
int axis_mode = 1; //1-單軸,2-雙軸

//判斷adjust使用
int adflag = 1; //1表示adjust可以使用，0為不行

//PID control
int targetCount = 0;
int targetCount2 = 0;
int targetCount3 = 0;
int curCount = 0;
int curCount2 = 0;
int curCount3 = 0;
long integral;
long integral2;
long integral3;
const int KiActivationErrorRange = 100;
const int KiRange2 = 10;
const float KiRange2Factor = 2;
const int KdActivationErrorRange = 100;

const float Kp = 2;
const float Ki = 0.00001;
const float Kd = 0; // negative here to compensate for different speeds when approaching target

//Encoder Variables
const unsigned short acceptableCountError = 0;
const unsigned short activationCountError = 1;
bool motorActive;
bool motor2Active;
bool motor3Active;
bool encoder1OutA_CurState, encoder1OutB_CurState, encoder1OutA_PrevState, encoder1OutB_PrevState;
bool encoder2OutA_CurState, encoder2OutB_CurState, encoder2OutA_PrevState, encoder2OutB_PrevState;
bool encoder3OutA_CurState, encoder3OutB_CurState, encoder3OutA_PrevState, encoder3OutB_PrevState;

void setup()
{
  Serial.begin(115200);

//第一組
  pinMode(M1_pinPWMA, OUTPUT);
  pinMode(M1_pinAIN1, OUTPUT);
  pinMode(M1_pinAIN2, OUTPUT);

  pinMode(M2_pinPWMA, OUTPUT);
  pinMode(M2_pinAIN1, OUTPUT);
  pinMode(M2_pinAIN2, OUTPUT);

  pinMode(pinSTBY_1, OUTPUT);
  
  pinMode(M1_PinA, INPUT);
  pinMode(M1_PinB, INPUT);
  M1_rePA = digitalRead(M1_PinA);
  M1_rePB = digitalRead(M1_PinB);

  pinMode(M2_PinA, INPUT);
  pinMode(M2_PinB, INPUT);
  M2_rePA = digitalRead(M2_PinA);
  M2_rePB = digitalRead(M2_PinB);

  //第二組
  pinMode(M3_pinPWMA, OUTPUT);
  pinMode(M3_pinAIN1, OUTPUT);
  pinMode(M3_pinAIN2, OUTPUT);

  pinMode(M4_pinPWMA, OUTPUT);
  pinMode(M4_pinAIN1, OUTPUT);
  pinMode(M4_pinAIN2, OUTPUT);

  pinMode(pinSTBY_2, OUTPUT);
  
  pinMode(M3_PinA, INPUT);
  pinMode(M3_PinB, INPUT);
  M3_rePA = digitalRead(M3_PinA);
  M3_rePB = digitalRead(M3_PinB);

  pinMode(M4_PinA, INPUT);
  pinMode(M4_PinB, INPUT);
  M4_rePA = digitalRead(M4_PinA);
  M4_rePB = digitalRead(M4_PinB);

//第三組
  pinMode(M5_pinPWMA, OUTPUT);
  pinMode(M5_pinAIN1, OUTPUT);
  pinMode(M5_pinAIN2, OUTPUT);

  pinMode(M6_pinPWMA, OUTPUT);
  pinMode(M6_pinAIN1, OUTPUT);
  pinMode(M6_pinAIN2, OUTPUT);

  pinMode(pinSTBY_3, OUTPUT);
  
  pinMode(M5_PinA, INPUT);
  pinMode(M5_PinB, INPUT);
  M5_rePA = digitalRead(M5_PinA);
  M5_rePB = digitalRead(M5_PinB);

  pinMode(M6_PinA, INPUT);
  pinMode(M6_PinB, INPUT);
  M6_rePA = digitalRead(M6_PinA);
  M6_rePB = digitalRead(M6_PinB);

  //紀錄neutral狀態位置，以便回到neutral
  M1_neutral = M1_recounter;
  M2_neutral = M2_recounter;
  M3_neutral = M3_recounter;
  M4_neutral = M4_recounter;
  M5_neutral = M5_recounter;
  M6_neutral = M6_recounter;

  motor_init();
}

void loop()
{ 
  M1_rotary_encoder();
  M2_rotary_encoder();
  M3_rotary_encoder();
  M4_rotary_encoder();
  M5_rotary_encoder();
  M6_rotary_encoder();

//  readEncoder();
//  getusPassed();
//  getEncoderSpeed();
//  getVoltageFeedFromPID();
//  updateMotor();
//  updateMotor2();
//  updateMotor3();
  
  M_rotate(); //判斷M1,M2轉動情況
//  if(adflag == 1){
//    M_adjust(); //回正
//  }

  if (Serial.available() > 0){
    char inst;
    inst = Serial.read();
    Serial.print("Instruction: ");
    Serial.println(inst);

//     motorDrive(motor6, turnCCW, 150);
/*
      if(inst == 't'){
        M1_rotate(1);
      }
      else if(inst =='g'){
        M1_rotate(-1);
      }
*/

      
      if(inst == 'r'){
        char axis;
        axis = Serial.read();
        Serial.print("axis: ");
        Serial.println(axis);
        if (axis == 'x'){ //x軸，M5 + M6
          int readNumber = Serial.parseInt();
          Serial.println("readNumber:");
          Serial.println(readNumber);
          if(readNumber == NULL){
    //        Serial.println("null");
             readNumber = 1000; //一圈
          }
          float loopNumber = readNumber /1000.0f;
    //      Serial.println("loopNumber:");
    //      Serial.println(loopNumber,3);
          M5_pre_rotate(loopNumber);
          M6_pre_rotate(loopNumber);
//            M5_rotate(loopNumber);
//            M6_rotate(loopNumber);
        }
    
        else if (axis == 'y'){ //y軸，M3 + M4
          int readNumber = Serial.parseInt();
          Serial.println("readNumber:");
          Serial.println(readNumber);
          if(readNumber == NULL){
    //        Serial.println("null");
             readNumber = 1000; //一圈
          }
          float loopNumber = readNumber /1000.0f;
    //      Serial.println("loopNumber:");
    //      Serial.println(loopNumber,3);
          M3_pre_rotate(loopNumber);
          M4_pre_rotate(loopNumber);
        }
    
        else if (axis == 'z'){ //z軸，M1 + M2
          int readNumber = Serial.parseInt();
          Serial.println("readNumber:");
          Serial.println(readNumber);
          if(readNumber == NULL){
    //        Serial.println("null");
             readNumber = 1000; //一圈
          }
          float loopNumber = readNumber /1000.0f;
    //      Serial.println("loopNumber:");
    //      Serial.println(loopNumber,3);
          M1_pre_rotate(loopNumber);
          M2_pre_rotate(loopNumber);
        }
      }

    //loose x,y,z axis
    else if(inst == 'l'){
      char axis;
      axis = Serial.read();
      Serial.print("axis: ");
      Serial.println(axis);
      if (axis == 'x'){ //loose x axis
        M5_pre_rotate(-1.5f);
        M6_pre_rotate(-1.5f);
      }
      else if(axis == 'y'){ //loose y axis
        M3_pre_rotate(-1.5f);
        M4_pre_rotate(-1.5f);
      }
      else if(axis == 'z'){ //loose z axis
        M1_pre_rotate(-1.5f);
        M2_pre_rotate(-1.5f);
      }
    }

    //extend x,y,z axis
    else if(inst == 'e'){
      char axis;
      axis = Serial.read();
      Serial.print("axis: ");
      Serial.println(axis);
      if (axis == 'x'){ //loose x axis
        M5_pre_rotate(1.5f);
        M6_pre_rotate(1.5f);
      }
      else if(axis == 'y'){ //loose y axis
        M3_pre_rotate(1.5f);
        M4_pre_rotate(1.5f);
      }
      else if(axis == 'z'){ //loose z axis
        M1_pre_rotate(1.5f);
        M2_pre_rotate(1.5f);
      }
    }  

    else if (inst == 'b'){
      adflag = 0; //adjust不能用
      int motor = Serial.parseInt();
      int clockwise = Serial.parseInt();
      int rspeed = Serial.parseInt();
      Serial.println(motor);
      Serial.println(clockwise);
      Serial.println(rspeed);
      if (clockwise == 0){
        motorDrive(motor, turnCW, rspeed);
      }
      else if (clockwise == 1){
        motorDrive(motor, turnCCW, rspeed);
      }
    }

    else if (inst == 's'){
      motorBrake(motor1);
      motorBrake(motor2);
      motorBrake(motor3);
      motorBrake(motor4);
      motorBrake(motor5);
      motorBrake(motor6);
    }

   
    //application
    else if(inst == 'a'){
      char appNum;
      appNum = Serial.read();
      Serial.println(appNum);
      if(appNum == '1'){ //application 1 (pan flipping) （yz平面 => 拉x)
        char level;
        level = Serial.read();
        Serial.println();
        if(level == '1'){ //egg x14.5N(1.25)
          M5_pre_rotate(1.25f);
          M6_pre_rotate(1.25f);
        }
        else if(level == '2'){ //gyoza x10.5N(0.5)
          M5_pre_rotate(0.5f);
          M6_pre_rotate(0.5f);
        }
        else if(level == '3'){ //steak x6.5N(0)
          reset();
        }
      }
      else if(appNum == '2'){ //application 2(shaker) (z平面 => 拉xy)
        char level;
        level = Serial.read();
        Serial.println();
        if(level == '1'){ //shaker1 xy8.5N(0)
          reset();
        }
        else if(level == '2'){ //shaker2 xy10.5N(0.3)
          M5_pre_rotate(0.3f);
          M6_pre_rotate(0.3f);
          M3_pre_rotate(0.3f);
          M4_pre_rotate(0.3f);
        }
        else if(level == '3'){ //shaker3 xy12.5N(0.45)
          M5_pre_rotate(0.45f);
          M6_pre_rotate(0.45f);
          M3_pre_rotate(0.45f);
          M4_pre_rotate(0.45f);
        }
        else if(level == '4'){ //shaker4 xy14.5N(0.725)
          M5_pre_rotate(0.725f);
          M6_pre_rotate(0.725f);
          M3_pre_rotate(0.725f);
          M4_pre_rotate(0.725f);
        }
        else if(level == '5'){ //shaker5 xy16.5N(1.25)
          M5_pre_rotate(1.25f);
          M6_pre_rotate(1.25f);
          M3_pre_rotate(1.25f);
          M4_pre_rotate(1.25f);
        }
      }
      else if(appNum == '3'){ //application 3(wine swirling) (xy平面 => 拉z)
        char level;
        level = Serial.read();
        Serial.println();
        if(level == '1'){ //wine1 z6.5N(0)
          reset();
        }
        else if(level == '2'){ //wine2 z10.5N(0.5)
          M1_pre_rotate(0.5f);
          M2_pre_rotate(0.5f);
        }
      }
      else if(appNum == '4'){ //fishing
        char level;
        level = Serial.read();
        Serial.println();
        if(level == '1'){ //big fish in water, xz(-1.5)
          M1_pre_rotate(-1.5f);
          M2_pre_rotate(-1.5f);
          M5_pre_rotate(-1.5f);
          M6_pre_rotate(-1.5f);
        }
        else if(level == '2'){ //big fish in air, yz(-1.5)
          M3_pre_rotate(-1.5f);
          M4_pre_rotate(-1.5f);
          M1_pre_rotate(-1.5f);
          M2_pre_rotate(-1.5f);
        }
        else if(level == '3'){ //small fish in water, xz(-1.5) y(0.5)
          M5_pre_rotate(-1.5f);
          M6_pre_rotate(-1.5f);
          M1_pre_rotate(-1.5f);
          M2_pre_rotate(-1.5f);
          M3_pre_rotate(0.5f);
          M4_pre_rotate(0.5f);
        }
        else if(level == '4'){ //small fish in air, yz(-1.5) x(0.5)
          M3_pre_rotate(-1.5f);
          M4_pre_rotate(-1.5f);
          M1_pre_rotate(-1.5f);
          M2_pre_rotate(-1.5f);
          M5_pre_rotate(0.5f);
          M6_pre_rotate(0.5f);
        }
        else if(level == '5'){ //none in water, xz(-1.5) y(1)
          M5_pre_rotate(-1.5f);
          M6_pre_rotate(-1.5f);
          M1_pre_rotate(-1.5f);
          M2_pre_rotate(-1.5f);
          M3_pre_rotate(1);
          M4_pre_rotate(1);
        }
        else if(level == '6'){ //none in air, yz(-1.5) x(1)
          M3_pre_rotate(-1.5f);
          M4_pre_rotate(-1.5f);
          M1_pre_rotate(-1.5f);
          M2_pre_rotate(-1.5f);
          M5_pre_rotate(1);
          M6_pre_rotate(1);
        }
      }
      else if(appNum == '5'){ //goldfishing
        char level;
        level = Serial.read();
        Serial.println();
        if(level == '1'){ //small, xz(1.25) y(-1.5)
          M5_pre_rotate(1.25f);
          M6_pre_rotate(1.25f);
          M1_pre_rotate(1.25f);
          M2_pre_rotate(1.25f);
          M3_pre_rotate(-1.5f);
          M4_pre_rotate(-1.5f);
        }
        else if(level == '2'){ //middle, xz(0.45) y(-1.5)
          M5_pre_rotate(0.45f);
          M6_pre_rotate(0.45f);
          M1_pre_rotate(0.45f);
          M2_pre_rotate(0.45f);
          M3_pre_rotate(-1.5f);
          M4_pre_rotate(-1.5f);
        }
        else if(level == '3'){ //big, y(-1.5)
          M3_pre_rotate(-1.5f);
          M4_pre_rotate(-1.5f);
        }
      }
      
    }


    else if (inst == 'c') { //正轉半圈
      char motorNum;
      motorNum = Serial.read();
      Serial.println(motorNum);
      if(motorNum == '1'){
        M1_pre_rotate(0.5f);
      }
      else if(motorNum == '2'){
        M2_pre_rotate(0.5f);
      }
      else if(motorNum == '3'){
        M3_pre_rotate(0.5f);
      }
      else if(motorNum == '4'){
        M4_pre_rotate(0.5f);
      }
      else if(motorNum == '5'){
        M5_pre_rotate(0.5f);
      }
      else if(motorNum == '6'){
        M6_pre_rotate(0.5f);
      }

    }
    else if (inst == 'd') { //逆轉半圈
      char motorNum;
      motorNum = Serial.read();
      Serial.println(motorNum);
      if(motorNum == '1'){
        M1_pre_rotate(-0.5f);
      }
      else if(motorNum == '2'){
        M2_pre_rotate(-0.5f);
      }
      else if(motorNum == '3'){
        M3_pre_rotate(-0.5f);
      }
      else if(motorNum == '4'){
        M4_pre_rotate(-0.5f);
      }
      else if(motorNum == '5'){
        M5_pre_rotate(-0.5f);
      }
      else if(motorNum == '6'){
        M6_pre_rotate(-0.5f);
      }
    }
    else if (inst == 'o'){ //重新設初始位置
      motor_init();
      adflag = 1;
      Serial.println(M1_init);
      Serial.println(M2_init);
      Serial.println(M3_init);
      Serial.println(M4_init);
      Serial.println(M5_init);
      Serial.println(M6_init);
    }
    else if(inst == 'p'){
      Serial.println(M1_trec);
      Serial.println(M2_trec);
    }
    else if (inst == 'h'){ //回到home
      reset();
    }
    else if(inst == 'n'){ //回到neutral位置與狀態
      M1_init = M1_neutral;
      M2_init = M2_neutral;
      M3_init = M3_neutral;
      M4_init = M4_neutral;
      M5_init = M5_neutral;
      M6_init = M6_neutral;

      M1_trec = M1_neutral;
      M2_trec = M2_neutral;
      M3_trec = M3_neutral;
      M4_trec = M4_neutral;
      M5_trec = M5_neutral;
      M6_trec = M6_neutral;

      reset();
    }
    
  }
}

void motorDrive(int motorNumber, boolean motorDirection, int motorSpeed)
{
  /*
  This Drives a specified motor, in a specific direction, at a specified speed:
    - motorNumber: motor1 or motor2 ---> Motor 1 or Motor 2
    - motorDirection: turnCW or turnCCW ---> clockwise or counter-clockwise
    - motorSpeed: 0 to 255 ---> 0 = stop / 255 = fast
  */

  boolean pinIn1;  //Relates to AIN1 or BIN1 (depending on the motor number specified)

 
//Specify the Direction to turn the motor
  //Clockwise: AIN1/BIN1 = HIGH and AIN2/BIN2 = LOW
  //Counter-Clockwise: AIN1/BIN1 = LOW and AIN2/BIN2 = HIGH
  if (motorDirection == turnCW){ //逆轉
    pinIn1 = HIGH;
  }
  else{ //正轉
    pinIn1 = LOW;
  }

//Select the motor to turn, and set the direction and the speed
  if(motorNumber == motor1)
  {
    digitalWrite(M1_pinAIN1, pinIn1);
    digitalWrite(M1_pinAIN2, !pinIn1);  //This is the opposite of the AIN1
    analogWrite(M1_pinPWMA, motorSpeed);
  }
  else if(motorNumber == motor2)
  {
    digitalWrite(M2_pinAIN1, pinIn1);
    digitalWrite(M2_pinAIN2, !pinIn1);  //This is the opposite of the BIN1
    analogWrite(M2_pinPWMA, motorSpeed);
  }
  else if(motorNumber == motor3)
  {
    digitalWrite(M3_pinAIN1, pinIn1);
    digitalWrite(M3_pinAIN2, !pinIn1);  //This is the opposite of the BIN1
    analogWrite(M3_pinPWMA, motorSpeed);
  }
  else if(motorNumber == motor4)
  {
    digitalWrite(M4_pinAIN1, pinIn1);
    digitalWrite(M4_pinAIN2, !pinIn1);  //This is the opposite of the BIN1
    analogWrite(M4_pinPWMA, motorSpeed);
  }
  else if(motorNumber == motor5)
  {
    digitalWrite(M5_pinAIN1, pinIn1);
    digitalWrite(M5_pinAIN2, !pinIn1);  //This is the opposite of the BIN1
    analogWrite(M5_pinPWMA, motorSpeed);
  }
  else if(motorNumber == motor6)
  {
    digitalWrite(M6_pinAIN1, pinIn1);
    digitalWrite(M6_pinAIN2, !pinIn1);  //This is the opposite of the BIN1
    analogWrite(M6_pinPWMA, motorSpeed);
  }
   
 

//Finally , make sure STBY is disabled - pull it HIGH
  digitalWrite(pinSTBY_1, HIGH);
  digitalWrite(pinSTBY_2, HIGH);
  digitalWrite(pinSTBY_3, HIGH);

}

void motorBrake(int motorNumber)
{
/*
This "Short Brake"s the specified motor, by setting speed to zero
*/
  
  if (motorNumber == motor1){
    analogWrite(M1_pinPWMA, 0);
  }
  else if(motorNumber == motor2){
    analogWrite(M2_pinPWMA, 0);
  }
  else if(motorNumber == motor3){
    analogWrite(M3_pinPWMA, 0);
  }
  else if(motorNumber == motor4){
    analogWrite(M4_pinPWMA, 0);
  }
  else if(motorNumber == motor5){
    analogWrite(M5_pinPWMA, 0);
  }
  else if(motorNumber == motor6){
    analogWrite(M6_pinPWMA, 0);
  }
  
}

void motorStop(int motorNumber)
{
  /*
  This stops the specified motor by setting both IN pins to LOW
  */
  
  if (motorNumber == motor1) {
    digitalWrite(M1_pinAIN1, LOW);
    digitalWrite(M1_pinAIN2, LOW);
  }
  else if(motorNumber == motor2)
  {
    digitalWrite(M2_pinAIN1, LOW);
    digitalWrite(M2_pinAIN2, LOW);
  } 
  else if(motorNumber == motor3)
  {
    digitalWrite(M3_pinAIN1, LOW);
    digitalWrite(M3_pinAIN2, LOW);
  }
  else if(motorNumber == motor4)
  {
    digitalWrite(M4_pinAIN1, LOW);
    digitalWrite(M4_pinAIN2, LOW);
  }
  else if(motorNumber == motor5)
  {
    digitalWrite(M5_pinAIN1, LOW);
    digitalWrite(M5_pinAIN2, LOW);
  }
  else if(motorNumber == motor6)
  {
    digitalWrite(M6_pinAIN1, LOW);
    digitalWrite(M6_pinAIN2, LOW);
  }
}


void motorsStandby()
{
  /*
  This puts the motors into Standby Mode
  */
  digitalWrite(pinSTBY_1, LOW);
  digitalWrite(pinSTBY_2, LOW);
  digitalWrite(pinSTBY_3, LOW);
}

void motor_init(){ //記錄初始位置
  M1_rotary_encoder();
  M2_rotary_encoder();
  M3_rotary_encoder();
  M4_rotary_encoder();
  M5_rotary_encoder();
  M6_rotary_encoder();
  
  M1_init = M1_recounter;
  M2_init = M2_recounter;
  M3_init = M3_recounter;
  M4_init = M4_recounter;
  M5_init = M5_recounter;
  M6_init = M6_recounter;

  M1_trec = M1_recounter;
  M2_trec = M2_recounter;
  M3_trec = M3_recounter;
  M4_trec = M4_recounter;
  M5_trec = M5_recounter;
  M6_trec = M6_recounter;
  

}
 
void reset(){ //回到初始值(neutral)
  // M1
  if(M1_recounter > M1_init || M1_recounter < M1_init){
    M1_state = 1;
    M1_trec = M1_init;
    if(M1_recounter > M1_init){ //要逆轉
      M1_mflag = -1;
      motorDrive(motor1, turnCW, 255); 
    }
    else if(M1_recounter < M1_init ){ //要正轉
      M1_mflag = 1;
      motorDrive(motor1, turnCCW, 255);
    }
  }

   // M2
  if(M2_recounter > M2_init || M2_recounter < M2_init){
    M2_state = 1;
    M2_trec = M2_init;
    if(M2_recounter > M2_init){ //要逆轉
      M2_mflag = -1;
      motorDrive(motor2, turnCW, 255); 
    }
    else if(M2_recounter < M2_init ){ //要正轉
      M2_mflag = 1;
      motorDrive(motor2, turnCCW, 255);
    }
  }

   // M3
  if(M3_recounter > M3_init || M3_recounter < M3_init){
    M3_state = 1;
    M3_trec = M3_init;
    if(M3_recounter > M3_init){ //要逆轉
      M3_mflag = -1;
      motorDrive(motor3, turnCW, 255); 
    }
    else if(M3_recounter < M3_init ){ //要正轉
      M3_mflag = 1;
      motorDrive(motor3, turnCCW, 255);
    }
  }

   // M4
  if(M4_recounter > M4_init || M4_recounter < M4_init){
    M4_state = 1;
    M4_trec = M4_init;
    if(M4_recounter > M4_init){ //要逆轉
      M4_mflag = -1;
      motorDrive(motor4, turnCW, 255); 
    }
    else if(M4_recounter < M4_init ){ //要正轉
      M4_mflag = 1;
      motorDrive(motor4, turnCCW, 255);
    }
  }

   // M5
  if(M5_recounter > M5_init || M5_recounter < M5_init){
    M5_state = 1;
    M5_trec = M5_init;
    if(M5_recounter > M5_init){ //要逆轉
      M5_mflag = -1;
      motorDrive(motor5, turnCW, 255); 
    }
    else if(M5_recounter < M5_init ){ //要正轉
      M5_mflag = 1;
      motorDrive(motor5, turnCCW, 255);
    }
  }

   // M6
  if(M6_recounter > M6_init || M6_recounter < M6_init){
    M6_state = 1;
    M6_trec = M6_init;
    if(M6_recounter > M6_init){ //要逆轉
      M6_mflag = -1;
      motorDrive(motor6, turnCW, 255); 
    }
    else if(M6_recounter < M6_init ){ //要正轉
      M6_mflag = 1;
      motorDrive(motor6, turnCCW, 255);
    }
  }
}

//void M_adjust(){
//  //馬達回正
//  int e1 = error;
//  int e2 = -error;
//  
//  if(M1_state == 0){ //M1
////    delay(60);
//    if(M1_recounter - M1_trec > e1 || M1_recounter - M1_trec < e2){
////      Serial.println("M1_recounter:");
////      Serial.println(M1_recounter);
////      Serial.println("M1_trec:");
////      Serial.println(M1_trec);
////      Serial.println();  
//   
//      //馬達轉超過，用較慢速度回正
//      if(M1_recounter - M1_trec > e1){ //正轉超過
////        Serial.println("正轉超過");
//        M1_mflag = -1;
////        motorDrive(motor1, turnCW, 100);
//        motorDrive(motor1, turnCW, 30); //逆轉
//      }
//      else if(M1_recounter - M1_trec < e2){ //逆轉超過
////        Serial.println("逆轉超過");
//        M1_mflag = 1;
//        motorDrive(motor1, turnCCW, 30); //正轉
//      } 
//    }
//    
//    if(M1_recounter - M1_trec < e1 && M1_recounter - M1_trec > e2){ //剛好
////      Serial.println("M1_recounter:");
////      Serial.println(M1_recounter);
////      Serial.println("M1_trec:");
////      Serial.println(M1_trec);
////      Serial.println();  
////      Serial.println("剛好！");
//      M1_mflag = 0;
//      motorBrake(motor1);
//    } 
//  }
//  
//  if(M2_state == 0){ //M2
////    delay(60);
//    if(M2_recounter - M2_trec > e1 || M2_recounter - M2_trec < e2){
//      //馬達轉超過，用較慢速度回正
//      if(M2_recounter - M2_trec > e1){ //正轉超過
////        Serial.println("正轉超過");
//        M2_mflag = -1;
//        motorDrive(motor2, turnCW, 30); //逆轉
//      }
//      else if(M2_recounter - M2_trec < e2){ //逆轉超過
////        Serial.println("逆轉超過");
//        M2_mflag = 1;
//        motorDrive(motor2, turnCCW, 30); //正轉
//      } 
//    }
//    
//    if(M2_recounter - M2_trec < e1 && M2_recounter - M2_trec > e2){ //剛好
////      Serial.println("剛好！");
//      M2_mflag = 0;
//      motorBrake(motor2);
//    }
//  }
//
//  if(M3_state == 0){ //M3
////    delay(60);
//    if(M3_recounter - M3_trec > e1 || M3_recounter - M3_trec < e2){
//      //馬達轉超過，用較慢速度回正
//      if(M3_recounter - M3_trec > e1){ //正轉超過
////        Serial.println("正轉超過");
//        M3_mflag = -1;
//        motorDrive(motor3, turnCW, 30); //逆轉
//      }
//      else if(M3_recounter - M3_trec < e2){ //逆轉超過
////        Serial.println("逆轉超過");
//        M3_mflag = 1;
//        motorDrive(motor3, turnCCW, 30); //正轉
//      } 
//    }
//    
//    if(M3_recounter - M3_trec < e1 && M3_recounter - M3_trec > e2){ //剛好
////      Serial.println("剛好！");
//      M3_mflag = 0;
//      motorBrake(motor3);
//    }
//  }
//
//  if(M4_state == 0){ //M4
////    delay(60);
//    if(M4_recounter - M4_trec > e1 || M4_recounter - M4_trec < e2){
//      //馬達轉超過，用較慢速度回正
//      if(M4_recounter - M4_trec > e1){ //正轉超過
////        Serial.println("正轉超過");
//        M4_mflag = -1;
//        motorDrive(motor4, turnCW, 30); //逆轉
//      }
//      else if(M4_recounter - M4_trec < e2){ //逆轉超過
////        Serial.println("逆轉超過");
//        M4_mflag = 1;
//        motorDrive(motor4, turnCCW, 30); //正轉
//      } 
//    }
//    
//    if(M4_recounter - M4_trec < e1 && M4_recounter - M4_trec > e2){ //剛好
////      Serial.println("剛好！");
//      M4_mflag = 0;
//      motorBrake(motor4);
//    }
//  }
//
//  if(M5_state == 0){ //M5
////    delay(60);
//    if(M5_recounter - M5_trec > e1 || M5_recounter - M5_trec < e2){
//      //馬達轉超過，用較慢速度回正
//      if(M5_recounter - M5_trec > e1){ //正轉超過
////        Serial.println("正轉超過");
//        M5_mflag = -1;
//        motorDrive(motor5, turnCW, 30); //逆轉
//      }
//      else if(M5_recounter - M5_trec < e2){ //逆轉超過
////        Serial.println("逆轉超過");
//        M5_mflag = 1;
//        motorDrive(motor5, turnCCW, 30); //正轉
//      } 
//    }
//    
//    if(M5_recounter - M5_trec < e1 && M5_recounter - M5_trec > e2){ //剛好
////      Serial.println("剛好！");
//      M5_mflag = 0;
//      motorBrake(motor5);
//    }
//  }
//
//  if(M6_state == 0){ //M6
////    delay(60);
//    if(M6_recounter - M6_trec > e1 || M6_recounter - M6_trec < e2){
//      //馬達轉超過，用較慢速度回正
//      if(M6_recounter - M6_trec > e1){ //正轉超過
////        Serial.println("正轉超過");
//        M6_mflag = -1;
//        motorDrive(motor6, turnCW, 30); //逆轉
//      }
//      else if(M6_recounter - M6_trec < e2){ //逆轉超過
////        Serial.println("逆轉超過");
//        M6_mflag = 1;
//        motorDrive(motor6, turnCCW, 30); //正轉
//      } 
//    }
//    
//    if(M6_recounter - M6_trec < e1 && M6_recounter - M6_trec > e2){ //剛好
////      Serial.println("剛好！");
//      M6_mflag = 0;
//      motorBrake(motor6);
//    }
//  }
//}

void M_rotate(){
  
  //馬達轉動
  if(M1_state == 1){ //M1
    if(M1_mflag == 1){ //正轉
      if(M1_recounter > M1_trec){ //轉到目標距離才停下
        motorBrake(motor1); //停下
        M1_mflag = 0;
        M1_state = 0;
      }
    }
    else if(M1_mflag == -1){ //逆轉
      if(M1_recounter < M1_trec){ //轉到目標距離才停下
        motorBrake(motor1); //停下
        M1_mflag = 0;
        M1_state = 0;
      }
    }
  }
  
  if(M2_state == 1){//M2
    if(M2_mflag == 1){ //正轉
      if(M2_recounter > M2_trec){ //轉到目標距離才停下
        motorBrake(motor2); //停下
        M2_mflag = 0;
        M2_state = 0;
      }
    }
    else if(M2_mflag == -1){ //逆轉
      if(M2_recounter < M2_trec){ //轉到目標距離才停下
        motorBrake(motor2); //停下
        M2_mflag = 0;
        M2_state = 0;
      }
    }
  }

  if(M3_state == 1){//M3
    if(M3_mflag == 1){ //正轉
      if(M3_recounter > M3_trec){ //轉到目標距離才停下
        motorBrake(motor3); //停下
        M3_mflag = 0;
        M3_state = 0;
      }
    }
    else if(M3_mflag == -1){ //逆轉
      if(M3_recounter < M3_trec){ //轉到目標距離才停下
        motorBrake(motor3); //停下
        M3_mflag = 0;
        M3_state = 0;
      }
    }
  }

  if(M4_state == 1){//M4
    if(M4_mflag == 1){ //正轉
      if(M4_recounter > M4_trec){ //轉到目標距離才停下
        motorBrake(motor4); //停下
        M4_mflag = 0;
        M4_state = 0;
      }
    }
    else if(M4_mflag == -1){ //逆轉
      if(M4_recounter < M4_trec){ //轉到目標距離才停下
        motorBrake(motor4); //停下
        M4_mflag = 0;
        M4_state = 0;
      }
    }
  }

  if(M5_state == 1){//M5
    if(M5_mflag == 1){ //正轉
      if(M5_recounter > M5_trec){ //轉到目標距離才停下
        motorBrake(motor5); //停下
        M5_mflag = 0;
        M5_state = 0;
      }
    }
    else if(M5_mflag == -1){ //逆轉
      if(M5_recounter < M5_trec){ //轉到目標距離才停下
        motorBrake(motor5); //停下
        M5_mflag = 0;
        M5_state = 0;
      }
    }
  }

  if(M6_state == 1){//M6
    if(M6_mflag == 1){ //正轉
      if(M6_recounter > M6_trec){ //轉到目標距離才停下
        motorBrake(motor6); //停下
        M6_mflag = 0;
        M6_state = 0;
      }
    }
    else if(M6_mflag == -1){ //逆轉
      if(M6_recounter < M6_trec){ //轉到目標距離才停下
        motorBrake(motor6); //停下
        M6_mflag = 0;
        M6_state = 0;
      }
    }
  }
}

void M1_pre_rotate(float loopnumber){ //設定M1相關變數
  M1_rotary_encoder();
  long crec = M1_recounter;
  M1_trec = crec + (loopnumber * cpr * gearratio);
//  Serial.println("M1_pre_rotate:");
//  Serial.println(crec);
//  Serial.println("M1_trec:");
//  Serial.println(M1_trec);

  integral = 0; //reset integral in PID control
  motorActive = true;
  
  if (loopnumber >= 0){ //正轉
    M1_mflag = 1;
    motorDrive(motor1, turnCCW, 255); 
  }
  else{
    M1_mflag = -1;
    motorDrive(motor1, turnCW, 255);
  }
  M1_state = 1;
}

void M2_pre_rotate(float loopnumber){ //設定M2相關變數
  M2_rotary_encoder();
  long crec = M2_recounter;
  M2_trec = crec + (loopnumber * cpr * gearratio);
  if (loopnumber >= 0){ //正轉
    M2_mflag = 1;
    motorDrive(motor2, turnCCW, 255);
  }
  else{
    M2_mflag = -1;
    motorDrive(motor2, turnCW, 255);
  }
  M2_state = 1;
}

void M3_pre_rotate(float loopnumber){ //設定M3相關變數
  M3_rotary_encoder();
  long crec = M3_recounter;
  M3_trec = crec + (loopnumber * cpr * gearratio);
  if (loopnumber >= 0){ //正轉
    M3_mflag = 1;
    motorDrive(motor3, turnCCW, 255);
  }
  else{
    M3_mflag = -1;
    motorDrive(motor3, turnCW, 255);
  }
  M3_state = 1;
}

void M4_pre_rotate(float loopnumber){ //設定M4相關變數
  M4_rotary_encoder();
  long crec = M4_recounter;
  M4_trec = crec + (loopnumber * cpr * gearratio);
  if (loopnumber >= 0){ //正轉
    M4_mflag = 1;
    motorDrive(motor4, turnCCW, 255);
  }
  else{
    M4_mflag = -1;
    motorDrive(motor4, turnCW, 255);
  }
  M4_state = 1;
}

void M5_pre_rotate(float loopnumber){ //設定M2相關變數
  M5_rotary_encoder();
  long crec = M5_recounter;
  M5_trec = crec + (loopnumber * cpr * gearratio);
  if (loopnumber >= 0){ //正轉
    M5_mflag = 1;
    motorDrive(motor5, turnCCW, 255);
  }
  else{
    M5_mflag = -1;
    motorDrive(motor5, turnCW, 255);
  }
  M5_state = 1;
}

void M6_pre_rotate(float loopnumber){ //設定M2相關變數
  M6_rotary_encoder();
  long crec = M6_recounter;
  M6_trec = crec + (loopnumber * cpr * gearratio);
  if (loopnumber >= 0){ //正轉
    M6_mflag = 1;
    motorDrive(motor6, turnCCW, 255);
  }
  else{
    M6_mflag = -1;
    motorDrive(motor6, turnCW, 255);
  }
  M6_state = 1;
}

void updateMotor() {
  //Serial.println("updateMotor");
  if (voltageFeed > 0) {
    motorDrive(motor1, turnDirection, voltageFeed);
  } else if (voltageFeed < 0) {
    motorDrive(motor1, !turnDirection, -voltageFeed);
  }
  else  {
    motorActive = false;
    motorStop(motor1);
    motorsStandby();
  }
}

void updateMotor2() {
  if (voltageFeed2 > 0) {
    motorDrive(motor2, turnDirection2, voltageFeed2);
  } else if (voltageFeed2 < 0) {
    motorDrive(motor2, !turnDirection2, -voltageFeed2);
  }
  else  {
    motor2Active = false;
    motorStop(motor2);
    motorsStandby();
  }
}


void updateMotor3() {
  if (voltageFeed3 > 0) {
    //Serial.println(motor3);
    motorDrive(motor3, turnDirection3, voltageFeed3);
  } else if (voltageFeed3 < 0) {
    motorDrive(motor3, !turnDirection3, -voltageFeed3);
  }
  else  {
    motor3Active = false;
    motorStop(motor3);
    motorsStandby();
  }
}

void M1_rotate(float number){  
  nowPosition1 = nowPosition1 + number;
  moveToBalanceLevel_1(nowPosition1);
}
void M2_rotate(float number){
  nowPosition2 = nowPosition2 + number;
  moveToBalanceLevel_2(nowPosition2);
}
void M3_rotate(float number){
  nowPosition3 = nowPosition3 + number;
  moveToBalanceLevel_3(nowPosition3);
}

void moveToBalanceLevel_1(float bal)
{
  Serial.println(bal);
  targetCount = bal * encoderValue;
  integral = 0; //reset integral in PID control
  motorActive = true;
}
void moveToBalanceLevel_2(float bal)
{
  Serial.println(bal);
  targetCount2 = bal * encoderValue;
  integral2 = 0; //reset integral in PID control
  motor2Active = true;
}
void moveToBalanceLevel_3(float bal)
{
  Serial.println(bal);
  targetCount3 = bal * encoderValue;
  integral3 = 0; //reset integral in PID control
  motor3Active = true;
}

void readEncoder()
{

  newPosition1 = Enc1.read();
  if (newPosition1 != oldPosition1) {
    oldPosition1 = newPosition1;
    Serial.println(newPosition1);
  }
  
  newPosition2 = Enc2.read();
  if (newPosition2 != oldPosition2) {
    oldPosition2 = newPosition2;
    Serial.println(newPosition2);
  }

  newPosition3 = Enc3.read();
  if (newPosition3 != oldPosition3) {
    oldPosition3 = newPosition3;
    Serial.println(newPosition3);
  }

}

void M1_rotary_encoder() {
  M1_reA = digitalRead(M1_PinA); // Reads the "current" state of the outputA
  M1_reB = digitalRead(M1_PinB);
  
  // If the previous and the current state of the outputA are different, that means a Pulse has occured
  if (M1_reA != M1_rePA || M1_reB != M1_rePB) {

    if(M1_mflag == 0){ //一般狀態
      // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
      if(M1_reA != M1_rePA && M1_reB != M1_rePB){ // rotate too fast, miss 1 change
        M1_recounter += 2;
      }
      else if (M1_reA == M1_rePB) {
        M1_recounter ++;
      } else {
        M1_recounter --;
      }
//      Serial.print("A: ");
//      Serial.print(M1_reA);
//      Serial.print(", ");
//      Serial.print("B: ");
//      Serial.println(M1_reB);
//      Serial.print("Position: ");
//      Serial.println(M1_recounter);
    }
    
    else if(M1_mflag == 1){ //正轉
      // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
      if(M1_reA != M1_rePA && M1_reB != M1_rePB){ // rotate too fast, miss 1 change
        M1_recounter += 2;
      }
      else {
        M1_recounter ++;
      }
    }

    else if(M1_mflag == -1){ //逆轉
      // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
      if(M1_reA != M1_rePA && M1_reB != M1_rePB){ // rotate too fast, miss 1 change
        M1_recounter -= 2;
      }
      else {
        M1_recounter --;
      }
    }  
  }
  
//  Serial.print("A: ");
//  Serial.print(M1_reA);
//  Serial.print(", ");
//  Serial.print("B: ");
//  Serial.println(M1_reB);
//  Serial.print("Position: ");
//  Serial.println(M1_recounter);
//  
  
  M1_rePA = M1_reA; // Updates the previous state of the outputA with the current state
  M1_rePB = M1_reB;
}

void M2_rotary_encoder() {
  M2_reA = digitalRead(M2_PinA); // Reads the "current" state of the outputA
  M2_reB = digitalRead(M2_PinB);
  
  // If the previous and the current state of the outputA are different, that means a Pulse has occured
  if (M2_reA != M2_rePA || M2_reB != M2_rePB) {

    if(M2_mflag == 0){
      // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
      if(M2_reA != M2_rePA && M2_reB != M2_rePB){ // rotate too fast, miss 1 change
        M2_recounter += 2;
      }
      else if (M2_reA == M2_rePB) {
        M2_recounter ++;
      } else {
        M2_recounter --;
      }
//
//      Serial.print("A: ");
//      Serial.print(M2_reA);
//      Serial.print(", ");
//      Serial.print("B: ");
//      Serial.println(M2_reB);
//      Serial.print("Position: ");
//      Serial.println(M2_recounter);
    }
    
    else if(M2_mflag == 1){
      // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
      if(M2_reA != M2_rePA && M2_reB != M2_rePB){ // rotate too fast, miss 1 change
        M2_recounter += 2;
      }
      else {
        M2_recounter ++;
      }
    }

    else if(M2_mflag == -1){
      // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
      if(M2_reA != M2_rePA && M2_reB != M2_rePB){ // rotate too fast, miss 1 change
        M2_recounter -= 2;
      }
      else {
        M2_recounter --;
      }
    }  
    
  }
  
  M2_rePA = M2_reA; // Updates the previous state of the outputA with the current state
  M2_rePB = M2_reB;
}


void M3_rotary_encoder() {
  M3_reA = digitalRead(M3_PinA); // Reads the "current" state of the outputA
  M3_reB = digitalRead(M3_PinB);
  
  // If the previous and the current state of the outputA are different, that means a Pulse has occured
  if (M3_reA != M3_rePA || M3_reB != M3_rePB) {

    if(M3_mflag == 0){
      // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
      if(M3_reA != M3_rePA && M3_reB != M3_rePB){ // rotate too fast, miss 1 change
        M3_recounter += 2;
      }
      else if (M3_reA == M3_rePB) {
        M3_recounter ++;
      } else {
        M3_recounter --;
      }

//      Serial.print("A: ");
//      Serial.print(M3_reA);
//      Serial.print(", ");
//      Serial.print("B: ");
//      Serial.println(M3_reB);
//      Serial.print("Position: ");
//      Serial.println(M3_recounter);
    }
    
    else if(M3_mflag == 1){
      // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
      if(M3_reA != M3_rePA && M3_reB != M3_rePB){ // rotate too fast, miss 1 change
        M3_recounter += 2;
      }
      else {
        M3_recounter ++;
      }
    }

    else if(M3_mflag == -1){
      // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
      if(M3_reA != M3_rePA && M3_reB != M3_rePB){ // rotate too fast, miss 1 change
        M3_recounter -= 2;
      }
      else {
        M3_recounter --;
      }
    }  
    
  }
  
  M3_rePA = M3_reA; // Updates the previous state of the outputA with the current state
  M3_rePB = M3_reB;
}

void M4_rotary_encoder() {
  M4_reA = digitalRead(M4_PinA); // Reads the "current" state of the outputA
  M4_reB = digitalRead(M4_PinB);
  
  // If the previous and the current state of the outputA are different, that means a Pulse has occured
  if (M4_reA != M4_rePA || M4_reB != M4_rePB) {

    if(M4_mflag == 0){
      // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
      if(M4_reA != M4_rePA && M4_reB != M4_rePB){ // rotate too fast, miss 1 change
        M4_recounter += 2;
      }
      else if (M4_reA == M4_rePB) {
        M4_recounter ++;
      } else {
        M4_recounter --;
      }

//      Serial.print("A: ");
//      Serial.print(M4_reA);
//      Serial.print(", ");
//      Serial.print("B: ");
//      Serial.println(M4_reB);
//      Serial.print("Position: ");
//      Serial.println(M4_recounter);
    }
    
    else if(M4_mflag == 1){
      // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
      if(M4_reA != M4_rePA && M4_reB != M4_rePB){ // rotate too fast, miss 1 change
        M4_recounter += 2;
      }
      else {
        M4_recounter ++;
      }
    }

    else if(M4_mflag == -1){
      // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
      if(M4_reA != M4_rePA && M4_reB != M4_rePB){ // rotate too fast, miss 1 change
        M4_recounter -= 2;
      }
      else {
        M4_recounter --;
      }
    }  
    
  }
  
  M4_rePA = M4_reA; // Updates the previous state of the outputA with the current state
  M4_rePB = M4_reB;
}

void M5_rotary_encoder() {
  M5_reA = digitalRead(M5_PinA); // Reads the "current" state of the outputA
  M5_reB = digitalRead(M5_PinB);
  
  // If the previous and the current state of the outputA are different, that means a Pulse has occured
  if (M5_reA != M5_rePA || M5_reB != M5_rePB) {

    if(M5_mflag == 0){
      // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
      if(M5_reA != M5_rePA && M5_reB != M5_rePB){ // rotate too fast, miss 1 change
        M5_recounter += 2;
      }
      else if (M5_reA == M5_rePB) {
        M5_recounter ++;
      } else {
        M5_recounter --;
      }

//      Serial.print("A: ");
//      Serial.print(M5_reA);
//      Serial.print(", ");
//      Serial.print("B: ");
//      Serial.println(M5_reB);
//      Serial.print("Position: ");
//      Serial.println(M5_recounter);
    }
    
    else if(M5_mflag == 1){
      // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
      if(M5_reA != M5_rePA && M5_reB != M5_rePB){ // rotate too fast, miss 1 change
        M5_recounter += 2;
      }
      else {
        M5_recounter ++;
      }
    }

    else if(M5_mflag == -1){
      // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
      if(M5_reA != M5_rePA && M5_reB != M5_rePB){ // rotate too fast, miss 1 change
        M5_recounter -= 2;
      }
      else {
        M5_recounter --;
      }
    }  
    
  }
  
  M5_rePA = M5_reA; // Updates the previous state of the outputA with the current state
  M5_rePB = M5_reB;
}

void M6_rotary_encoder() {
  M6_reA = digitalRead(M6_PinA); // Reads the "current" state of the outputA
  M6_reB = digitalRead(M6_PinB);
  
  // If the previous and the current state of the outputA are different, that means a Pulse has occured
  if (M6_reA != M6_rePA || M6_reB != M6_rePB) {

    if(M6_mflag == 0){
      // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
      if(M6_reA != M6_rePA && M6_reB != M6_rePB){ // rotate too fast, miss 1 change
        M6_recounter += 2;
      }
      else if (M6_reA == M6_rePB) {
        M6_recounter ++;
      } else {
        M6_recounter --;
      }

//      Serial.print("A: ");
//      Serial.print(M6_reA);
//      Serial.print(", ");
//      Serial.print("B: ");
//      Serial.println(M6_reB);
//      Serial.print("Position: ");
//      Serial.println(M6_recounter);
    }
    
    else if(M6_mflag == 1){
      // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
      if(M6_reA != M6_rePA && M6_reB != M6_rePB){ // rotate too fast, miss 1 change
        M6_recounter += 2;
      }
      else {
        M6_recounter ++;
      }
    }

    else if(M6_mflag == -1){
      // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
      if(M6_reA != M6_rePA && M6_reB != M6_rePB){ // rotate too fast, miss 1 change
        M6_recounter -= 2;
      }
      else {
        M6_recounter --;
      }
    }  
    
  }
  
  M6_rePA = M6_reA; // Updates the previous state of the outputA with the current state
  M6_rePB = M6_reB;
}

void getVoltageFeedFromPID()
{
  int error;
  float I, D;

  error = targetCount - newPosition1;
  // Integral
  if (abs(error) < KiActivationErrorRange) {
    integral += error * usPassed;
    if (abs(error) < KiRange2) {
      I = Ki * KiRange2Factor * integral;
    } else {
      I = Ki * integral;
    }
  } else {
    integral = 0;
    I = 0;
  }
  //Derivative
  if (abs(error) < KdActivationErrorRange) {
    D = Kd * encoderSpeed;
  } else {
    D = 0;
  }

  //Derive driving voltage
  if (abs(targetCount - newPosition1) > activationCountError) {
    motorActive = true;
  }
  if (abs(targetCount - newPosition1) > acceptableCountError && motorActive) { // after motor has reached acceptableCountError, the error needs to excceed activationCountError to start the motor again.
    voltageFeed = Kp * (error) + I + D;

    if (voltageFeed > 0) {
      if (voltageFeed > maximalVoltage) {
        voltageFeed = maximalVoltage;
      }
      //motorDrive(motor1, turnDirection, voltageFeed);
    } else {
      if (voltageFeed < -maximalVoltage) {
        voltageFeed = -maximalVoltage;
      }
      //motorDrive(motor1, !turnDirection, -voltageFeed);
    }
  } else {
    integral = 0;
    voltageFeed = 0;
    M1_complete = true;
  }

  /////////////////
  int error2;
  float I2, D2;

  error2 = targetCount2 - newPosition2;
  // Integral
  if (abs(error2) < KiActivationErrorRange) {
    integral2 += error2 * usPassed2;
    if (abs(error2) < KiRange2) {
      I2 = Ki * KiRange2Factor * integral2;
    } else {
      I2 = Ki * integral2;
    }
  } else {
    integral2 = 0;
    I2 = 0;
  }
  //Derivative
  if (abs(error2) < KdActivationErrorRange) {
    D2 = Kd * encoderSpeed2;
  } else {
    D2 = 0;
  }

  //Derive driving voltage
  if (abs(targetCount2 - newPosition2) > activationCountError) {
    motor2Active = true;
  }
  if (abs(targetCount2 - newPosition2) > acceptableCountError && motor2Active) { // after motor has reached acceptableCountError, the error needs to excceed activationCountError to start the motor again.
    voltageFeed2 = Kp * (error2) + I2 + D2;
    
    if (voltageFeed2 > 0) {
      if (voltageFeed2 > maximalVoltage) {
        voltageFeed2 = maximalVoltage;
      }
      //motorDrive(motor1, turnDirection, voltageFeed);
    } else {
      if (voltageFeed2 < -maximalVoltage) {
        voltageFeed2 = -maximalVoltage;
      }
      //motorDrive(motor1, !turnDirection, -voltageFeed);
    }
  } else {
    integral2 = 0;
    voltageFeed2 = 0;
  }

  /////////////////
  int error3;
  float I3, D3;

  error3 = targetCount3 - newPosition3;
  // Integral
  if (abs(error3) < KiActivationErrorRange) {
    integral3 += error3 * usPassed3;
    if (abs(error3) < KiRange2) {
      I3 = Ki * KiRange2Factor * integral3;
    } else {
      I3 = Ki * integral3;
    }
  } else {
    integral3 = 0;
    I3 = 0;
  }
  //Derivative
  if (abs(error3) < KdActivationErrorRange) {
    D3 = Kd * encoderSpeed3;
  } else {
    D3 = 0;
  }

  //Derive driving voltage
  if (abs(targetCount3 - newPosition3) > activationCountError) {
    motor3Active = true;
  }
  if (abs(targetCount3 - newPosition3) > acceptableCountError && motor3Active) { // after motor has reached acceptableCountError, the error needs to excceed activationCountError to start the motor again.
    voltageFeed3 = Kp * (error3) + I3 + D3;

    if (voltageFeed3 > 0) {
      if (voltageFeed3 > maximalVoltage) {
        voltageFeed3 = maximalVoltage;
      }
      //motorDrive(motor1, turnDirection, voltageFeed);
    } else {
      if (voltageFeed3 < -maximalVoltage) {
        voltageFeed3 = -maximalVoltage;
      }
      //motorDrive(motor1, !turnDirection, -voltageFeed);
    }
  } else {
    integral3 = 0;
    voltageFeed3 = 0;
  }
}

void getusPassed() {
  //Serial.println("getusPassed");
  static unsigned long prevTime = 0;
  unsigned long curTime;
  static unsigned long prevTime2 = 0;
  unsigned long curTime2;
  static unsigned long prevTime3 = 0;
  unsigned long curTime3;

  curTime = micros();
  usPassed = curTime - prevTime;
  prevTime = curTime;

  curTime2 = micros();
  usPassed2 = curTime2 - prevTime2;
  prevTime2 = curTime2;

  curTime3 = micros();
  usPassed3 = curTime3 - prevTime3;
  prevTime3 = curTime3;
}

void getEncoderSpeed() {
  //Serial.println("getEncoderSpeed");
  static unsigned long usPassedBetweenEncoderReadings = 0;
  static int prevCount = 0;
  float newSpeed;
  static unsigned long usPassed2BetweenEncoderReadings = 0;
  static int prevCount2 = 0;
  float newSpeed2;
  static unsigned long usPassed3BetweenEncoderReadings = 0;
  static int prevCount3 = 0;
  float newSpeed3;

  usPassedBetweenEncoderReadings += usPassed;
  if (usPassed > 1000) {
    newSpeed = (float)(curCount - prevCount) * 0.001;
    prevCount = curCount;
    usPassedBetweenEncoderReadings -= 1000;
    encoderSpeed = encoderSpeed * 0.8 + (newSpeed) * 0.2;
  }

  usPassed2BetweenEncoderReadings += usPassed2;
  if (usPassed2 > 1000) {
    newSpeed2 = (float)(curCount2 - prevCount2) * 0.001;
    prevCount2 = curCount2;
    usPassed2BetweenEncoderReadings -= 1000;
    encoderSpeed2 = encoderSpeed2 * 0.8 + (newSpeed2) * 0.2;
  }

  usPassed3BetweenEncoderReadings += usPassed3;
  if (usPassed3 > 1000) {
    newSpeed3 = (float)(curCount3 - prevCount3) * 0.001;
    prevCount3 = curCount3;
    usPassed3BetweenEncoderReadings -= 1000;
    encoderSpeed3 = encoderSpeed3 * 0.8 + (newSpeed3) * 0.2;
  }
}
