
#include <Encoder.h>

Encoder Enc1(2,22);//M1
Encoder Enc2(3,5);//M2
Encoder Enc3(18,17);//M3
Encoder Enc4(19,46);//M4
Encoder Enc5(20,31);//M5
Encoder Enc6(21,14);//M6

long newPosition1;
long oldPosition1  = -999;
long newPosition2;
long oldPosition2  = -999;
long newPosition3;
long oldPosition3  = -999;
long newPosition4;
long oldPosition4  = -999;
long newPosition5;
long oldPosition5  = -999;
long newPosition6;
long oldPosition6  = -999;
/*******Pin Definitions on Arduino UNO*******/
// Encoders
//const int encoder1OutA = 12;
//const int encoder1OutB = 13;
//const int encoder2OutA = 3;
//const int encoder2OutB = 4;
// Motors (connecting to TB6612FNG)
//Motor 1
const int M1_pinAIN1 = 26; //Direction
const int M1_pinAIN2 = 24; //Direction
const int M1_pinPWMA = 9; //Voltage

//Motor 2
const int M2_pinBIN1 = 45; //Direction
const int M2_pinBIN2 = 47; //Direction
const int M2_pinPWMB = 7; //Voltage

//Motor 3  
const int M3_pinAIN1 = 38; //Direction
const int M3_pinAIN2 = 36; //Direction
const int M3_pinPWMA = 11; //Voltage

//Motor 4 
const int M4_pinBIN1 = 42; //Direction
const int M4_pinBIN2 = 44; //Direction
const int M4_pinPWMB = 10; //Voltage

//Motor 5  
const int M5_pinAIN1 = 50; //Direction
const int M5_pinAIN2 = 48; //Direction
const int M5_pinPWMA = 13; //Voltage

//Motor 6  
const int M6_pinBIN1 = 53; //Direction
const int M6_pinBIN2 = 51; //Direction
const int M6_pinPWMB = 12; //Voltage

//Standby
const int pinSTBY = 28;
const int pinSTBY_2 = 40; //要改成共用寫法！
const int pinSTBY_3 = 52;

//Constants
//const int gearRatio = 211;

//Encoder Variables
const unsigned short acceptableCountError = 0;
const unsigned short activationCountError = 1;
bool motorActive;
bool motor2Active;
bool motor3Active;
bool motor4Active;
bool motor5Active;
bool motor6Active;
//bool encoder1OutA_CurState, encoder1OutB_CurState, encoder1OutA_PrevState, encoder1OutB_PrevState;
//bool encoder2OutA_CurState, encoder2OutB_CurState, encoder2OutA_PrevState, encoder2OutB_PrevState;
//bool encoder3OutA_CurState, encoder3OutB_CurState, encoder3OutA_PrevState, encoder3OutB_PrevState;

//PID control
int targetCount = 0;
int targetCount2 = 0;
int targetCount3 = 0;
int targetCount4 = 0;
int targetCount5 = 0;
int targetCount6 = 0;
int curCount = 0;
int curCount2 = 0;
int curCount3 = 0;
int curCount4 = 0;
int curCount5 = 0;
int curCount6 = 0;
long integral;
long integral2;
long integral3;
long integral4;
long integral5;
long integral6;
const int KiActivationErrorRange = 100;
const int KiRange2 = 10;
const float KiRange2Factor = 2;
const int KdActivationErrorRange = 100;
//tuning guidelines: (Voltage feed = Kp*(error) + Ki*error.usPassed + Kd*encoderSpeed)
//Kp: error approx 100(small step / 10 motor rounds) -> 10,000(big step) -> around 2.5E-1
//Ki: error approx 1.0E3, total us in transition approx 1.0E6 -> 2.5E-7
//Kd: approx 1.0E-1 encoder counts/us -> 2.5E+3
//const float Kp = 2.5e-1;
//const float Ki = 5e-7;
//const float Kd = -5e3; // negative here to compensate for different speeds when approaching target

const float Kp = 2;
const float Ki = 0.00001;
const float Kd = 0; // negative here to compensate for different speeds when approaching target

//global variables
unsigned long usPassed;
unsigned long usPassed2;
unsigned long usPassed3;
unsigned long usPassed4;
unsigned long usPassed5;
unsigned long usPassed6;
float encoderSpeed = 0;
float encoderSpeed2 = 0;
float encoderSpeed3 = 0;
float encoderSpeed4 = 0;
float encoderSpeed5 = 0;
float encoderSpeed6 = 0;

//Motor variables
int voltageFeed;
int voltageFeed2;
int voltageFeed3;
int voltageFeed4;
int voltageFeed5;
int voltageFeed6;
const bool turnDirection = true;  //for motorDrive function
const bool turnDirection2 = true;
const bool turnDirection3 = true;
const bool turnDirection4 = true;  //for motorDrive function
const bool turnDirection5 = true;
const bool turnDirection6 = true;
const int motor1 = 0;  //for motorDrive, motorStop, motorBrake functions
const int motor2 = 1;  //for motorDrive, motorStop, motorBrake functions
const int motor3 = 2;  //for motorDrive, motorStop, motorBrake functions
const int motor4 = 4;  //for motorDrive, motorStop, motorBrake functions
const int motor5 = 5;  //for motorDrive, motorStop, motorBrake functions
const int motor6 = 6;  //for motorDrive, motorStop, motorBrake functions

//String incomingStr;
//String vDirection;
//String hDirection;
//String Direction;
int encoderValue = 2532; // cpr(12) * gearRatio(211)
int maximalVoltage = 255;

float originalpoint = 0;
float nowPosition1 = 0; // 以圈數為單位
float nowPosition2 = 0; // 以圈數為單位
float nowPosition3 = 0; // 以圈數為單位
float nowPosition4 = 0; // 以圈數為單位
float nowPosition5 = 0; // 以圈數為單位
float nowPosition6 = 0; // 以圈數為單位

//neutral position
int M1_neutral = 0;
int M2_neutral = 0;
int M3_neutral = 0;
int M4_neutral = 0;
int M5_neutral = 0;
int M6_neutral = 0;

//flag
bool M_adjust = false;

void setup()
{
  Serial.begin(115200); //Sets the data rate in bits per second (baud) for serial data transmission. For communicating with the computer, use one of these rates: 300, 600, 1200, 2400, 4800, 9600, 19200, 28800, 38400, 57600, or 115200
  randomSeed(0);
  //Set the PIN Modes
  pinMode(M1_pinPWMA, OUTPUT);
  pinMode(M1_pinAIN1, OUTPUT);
  pinMode(M1_pinAIN2, OUTPUT);

  pinMode(M2_pinPWMB, OUTPUT);
  pinMode(M2_pinBIN1, OUTPUT);
  pinMode(M2_pinBIN2, OUTPUT);

  pinMode(pinSTBY, OUTPUT);
  digitalWrite(pinSTBY, HIGH);

  pinMode(M3_pinPWMA, OUTPUT);
  pinMode(M3_pinAIN1, OUTPUT);
  pinMode(M3_pinAIN2, OUTPUT);

  pinMode(M4_pinPWMB, OUTPUT);
  pinMode(M4_pinBIN1, OUTPUT);
  pinMode(M4_pinBIN2, OUTPUT);
  
  pinMode(pinSTBY_2, OUTPUT);
  digitalWrite(pinSTBY_2, HIGH);

  pinMode(M5_pinPWMA, OUTPUT);
  pinMode(M5_pinAIN1, OUTPUT);
  pinMode(M5_pinAIN2, OUTPUT);

  pinMode(M6_pinPWMB, OUTPUT);
  pinMode(M6_pinBIN1, OUTPUT);
  pinMode(M6_pinBIN2, OUTPUT);
  
  pinMode(pinSTBY_3, OUTPUT);
  digitalWrite(pinSTBY_3, HIGH);

//  pinMode(encoder1OutA, INPUT);
//  pinMode(encoder1OutB, INPUT);
//  pinMode(encoder2OutA, INPUT);
//  pinMode(encoder2OutB, INPUT);
  //record initial encoder state
//  encoder1OutA_PrevState = digitalRead(encoder1OutA);
//  encoder1OutB_PrevState = digitalRead(encoder1OutB);
//  encoder2OutA_PrevState = digitalRead(encoder2OutA);
//  encoder2OutB_PrevState = digitalRead(encoder2OutB);
}


void loop()
{
  
  readEncoder();
  getusPassed();
  getEncoderSpeed();
  if(M_adjust == false)  
    getVoltageFeedFromPID();
    
  updateMotor();
  updateMotor6();
  updateMotor5();
  updateMotor4();
  updateMotor3();
  updateMotor2(); //weird problem, need to be put in end  
  
}


char inst; 
void serialEvent() {
  inst = Serial.read();
  //Serial.print("Inst:");
  //Serial.println(inst);
  if(inst == 'p'){ //print all motors' now positions
    readEncoder();
    Serial.println(newPosition1);
    Serial.println(newPosition2);
    Serial.println(newPosition3);
    Serial.println(newPosition4);
    Serial.println(newPosition5);
    Serial.println(newPosition6);
  }
  else if(inst == 'h'){ //go home (initial position)
    reset(0,0,0,0,0,0);
  }
  else if(inst == 'o'){ //set new neutral position
    Serial.println("new neutral pos");
    readEncoder();
    M1_neutral = newPosition1;
    M2_neutral = newPosition2;
    M3_neutral = newPosition3;
    M4_neutral = newPosition4;
    M5_neutral = newPosition5;
    M6_neutral = newPosition6;
  }
  else if(inst == 'n'){ //go back to neutral position
    reset(M1_neutral,M2_neutral,M3_neutral,M4_neutral,M5_neutral,M6_neutral);
  }
  else if(inst == 'c'){
    int motorNum;
    motorNum = Serial.parseInt();
    if(motorNum == 1){
      M1_rotate(0.5f);
    }
    else if(motorNum == 2){
      M2_rotate(0.5f);
    }
    else if(motorNum == 3){
      M3_rotate(0.5f);
    }
    else if(motorNum == 4){
      M4_rotate(0.5f);
    }
    else if(motorNum == 5){
      M5_rotate(0.5f);
    }
    else if(motorNum == 6){
      M6_rotate(0.5f);
    }
  }
  
  else if(inst == 'd'){
    int motorNum;
    motorNum = Serial.parseInt();
    if(motorNum == 1){
      M1_rotate(-0.5f);
    }
    else if(motorNum == 2){
      M2_rotate(-0.5f);
    }
    else if(motorNum == 3){
      M3_rotate(-0.5f);
    }
    else if(motorNum == 4){
      M4_rotate(-0.5f);
    }
    else if(motorNum == 5){
      M5_rotate(-0.5f);
    }
    else if(motorNum == 6){
      M6_rotate(-0.5f);
    }
  }

  //rotate x,y,z in arbitrary speed
  else if (inst == 'x'){ //x軸，M5 + M6
      Serial.println(inst);
      int readNumber = Serial.parseInt();
      Serial.println("readNumber:");
      Serial.println(readNumber);
      if(readNumber == NULL){
        readNumber = 1000; //一圈
      }
      float loopNumber = readNumber /1000.0f;
      M5_rotate(loopNumber);
      M6_rotate(loopNumber);
  }

  else if (inst == 'y'){ //y軸，M3 + M4
      Serial.println(inst);
      int readNumber = Serial.parseInt();
      Serial.println("readNumber:");
      Serial.println(readNumber);
      if(readNumber == NULL){
        readNumber = 1000; //一圈
      }
      float loopNumber = readNumber /1000.0f;
      M3_rotate(loopNumber);
      M4_rotate(loopNumber);
  }

  else if (inst == 'z'){ //z軸，M1 + M2
      Serial.println(inst);
      int readNumber = Serial.parseInt();
      Serial.println("readNumber:");
      Serial.println(readNumber);
      if(readNumber == NULL){
         readNumber = 1000; //一圈
      }
      float loopNumber = readNumber /1000.0f;
      M1_rotate(loopNumber);
      M2_rotate(loopNumber);
  }

  else if (inst == 'b'){ //adjustment
      Serial.println("b");
      int motor = Serial.parseInt();
      int clockwise = Serial.parseInt();
      Serial.println(motor);
      Serial.println(clockwise);
      int rspeed;
      int rspeed2; //just for motor2
      if(clockwise == 1){
        rspeed = 30;
        rspeed2 = 30;
      }
      else if(clockwise == 0){
        rspeed = -30;
        rspeed2 = -100;
      }
      
      M_adjust = true;
      if(motor == 1){
        voltageFeed = rspeed;
        targetCount = 1 * encoderValue;
        integral = 0; //reset integral in PID control
        motorActive = true;
      }
      else if(motor == 2){
        
        voltageFeed2 = rspeed2;
        targetCount2 = 1 * encoderValue;
        integral2 = 0; //reset integral in PID control
        motor2Active = true;
      }
      else if(motor == 3){
        voltageFeed3 = rspeed;
        targetCount3 = 1 * encoderValue;
        integral3 = 0; //reset integral in PID control
        motor3Active = true;
      }
      else if(motor == 4){
        voltageFeed4 = rspeed;
        targetCount4 = 1 * encoderValue;
        integral4 = 0; //reset integral in PID control
        motor4Active = true;
      }
      else if(motor == 5){
        voltageFeed5 = rspeed;
        targetCount5 = 1 * encoderValue;
        integral5 = 0; //reset integral in PID control
        motor5Active = true;
      }
      else if(motor == 6){
        voltageFeed6 = rspeed;
        targetCount6 = 1 * encoderValue;
        integral6 = 0; //reset integral in PID control
        motor6Active = true;
      }
  }
  else if (inst == 's'){ //stop all motors
      Serial.println("stop");  
      motorBrake(motor1);
      motorBrake(motor2);
      motorBrake(motor3);
      motorBrake(motor4);
      motorBrake(motor5);
      motorBrake(motor6);
      motorsStandby();
      motorsStandby2();
      motorsStandby3();
      voltageFeed = 0;
      voltageFeed2 = 0;
      voltageFeed3 = 0;
      voltageFeed4 = 0;
      voltageFeed5 = 0;
      voltageFeed6 = 0;
      readEncoder();
      targetCount = newPosition1;
      targetCount2 = newPosition2;
      targetCount3 = newPosition3;
      targetCount4 = newPosition4;
      targetCount5 = newPosition5;
      targetCount6 = newPosition6;
      nowPosition1 = newPosition1;
      nowPosition2 = newPosition2;
      nowPosition3 = newPosition3;
      nowPosition4 = newPosition4;
      nowPosition5 = newPosition5;
      nowPosition6 = newPosition6;  
  }
  
  //VR application (晃動小->大)(緊->鬆)
  //application 1 (pan flipping) （yz平面 => 拉x)
  else if(inst == '0'){ //pan 1 , y-2500, z-2500, x-600
    M1_rotate(-2.5f);
    M2_rotate(-2.5f);
    M3_rotate(-2.5f);
    M4_rotate(-2.5f);
    M5_rotate(-0.6f);
    M6_rotate(-0.6f);
  }
  else if(inst == '1'){ //pan 2 , y-2500, z-2500, x-800
    M1_rotate(-2.5f);
    M2_rotate(-2.5f);
    M3_rotate(-2.5f);
    M4_rotate(-2.5f);
    M5_rotate(-0.8f);
    M6_rotate(-0.8f);
  }
  
  //application 2(shaker) (z平面 => 拉xy)
  else if(inst == '2'){ //shaker1 , z-2500, x-100, y-100
    M1_rotate(-2.5f);
    M2_rotate(-2.5f);
    M5_rotate(-0.1f);
    M6_rotate(-0.1f);
    M3_rotate(-0.1f);
    M4_rotate(-0.1f);
  }
  else if(inst == '3'){ //shaker2 , z-2500, x-500, y-500
    M1_rotate(-2.5f);
    M2_rotate(-2.5f);
    M5_rotate(-0.5f);
    M6_rotate(-0.5f);
    M3_rotate(-0.5f);
    M4_rotate(-0.5f);
  }
  
  //application 3(wine swirling) (xy平面 => 拉z)
  else if(inst == '4'){ //wine1 , x-2500, y-2500, z-200
    M5_rotate(-2.5f);
    M6_rotate(-2.5f);
    M3_rotate(-2.5f);
    M4_rotate(-2.5f);
    M1_rotate(-0.2f);
    M2_rotate(-0.2f);
  }
  else if(inst == '5'){ //wine2 , x-2500, y-2500, z-600
    M5_rotate(-2.5f);
    M6_rotate(-2.5f);
    M3_rotate(-2.5f);
    M4_rotate(-2.5f);
    M1_rotate(-0.6f);
    M2_rotate(-0.6f);
  }
  else if(inst == '6'){ //wine3 , x-2500, y-2500, z-800
    M5_rotate(-2.5f);
    M6_rotate(-2.5f);
    M3_rotate(-2.5f);
    M4_rotate(-2.5f);
    M1_rotate(-0.8f);
    M2_rotate(-0.8f);
  }
  
  //application 4(fishing)
  else if(inst == 'a'){ //big fish in water, xz(-2.5), y-800 
    M5_rotate(-2.5f);
    M6_rotate(-2.5f);
    M1_rotate(-2.5f);
    M2_rotate(-2.5f);
    M3_rotate(-0.8f);
    M4_rotate(-0.8f);
  }
  else if(inst == 'f'){ //big fish in air, yz(-2.5) , x-800 
    M3_rotate(-2.5f);
    M4_rotate(-2.5f);
    M1_rotate(-2.5f);
    M2_rotate(-2.5f);
    M5_rotate(-0.8f);
    M6_rotate(-0.8f);
  }
  else if(inst == 'g'){ //small fish in water, xz(-2.5) y-600
    M1_rotate(-2.5f);
    M2_rotate(-2.5f);
    M5_rotate(-2.5f);
    M6_rotate(-2.5f);
    M3_rotate(-0.6f);
    M4_rotate(-0.6f);
  }
  else if(inst == 'i'){ //small fish in air, yz(-2.5) x-600
    M3_rotate(-2.5f);
    M4_rotate(-2.5f);
    M1_rotate(-2.5f);
    M2_rotate(-2.5f);
    M5_rotate(-0.6f);
    M6_rotate(-0.6f);
  }
  else if(inst == 'j'){ //none in water, xz(-2.5) y-200
    M5_rotate(-2.5f);
    M6_rotate(-2.5f);
    M1_rotate(-2.5f);
    M2_rotate(-2.5f);
    M3_rotate(-0.2f);
    M4_rotate(-0.2f);
  }
  else if(inst == 'k'){ //none in air, yz(-2.5) x-200
    M3_rotate(-2.5f);
    M4_rotate(-2.5f);
    M1_rotate(-2.5f);
    M2_rotate(-2.5f);
    M5_rotate(-0.2f);
    M6_rotate(-0.2f);
  }
}

void reset(int pos1,int pos2,int pos3,int pos4,int pos5,int pos6){
  Serial.println("reset");
  //M1
  targetCount = pos1;
  nowPosition1 = pos1;
  integral = 0; //reset integral in PID control
  motorActive = true;
  //M2
  targetCount2 = pos2;
  nowPosition2 = pos2;
  integral2 = 0; //reset integral in PID control
  motor2Active = true;
  //M3
  targetCount3 = pos3;
  nowPosition3 = pos3;
  integral3 = 0; //reset integral in PID control
  motor3Active = true;
  //M4
  targetCount4 = pos4;
  nowPosition4 = pos4;
  integral4 = 0; //reset integral in PID control
  motor4Active = true;
  //M5
  targetCount5 = pos5;
  nowPosition5 = pos5;
  integral5 = 0; //reset integral in PID control
  motor5Active = true;
  //M6
  targetCount6 = pos6;
  nowPosition6 = pos6;
  integral6 = 0; //reset integral in PID control
  motor6Active = true;
}

void M1_rotate(float number){  
  nowPosition1 = nowPosition1 + number;
  moveToBalanceLevel_1(nowPosition1);
}
void M2_rotate(float number){
  nowPosition2 = nowPosition2 + number;
  moveToBalanceLevel_2(nowPosition2);
}
void M3_rotate(float number){
  nowPosition3 = nowPosition3 + number;
  moveToBalanceLevel_3(nowPosition3);
}
void M4_rotate(float number){
  nowPosition4 = nowPosition4 + number;
  moveToBalanceLevel_4(nowPosition4);
}
void M5_rotate(float number){
  nowPosition5 = nowPosition5 + number;
  moveToBalanceLevel_5(nowPosition5);
}
void M6_rotate(float number){
  nowPosition6 = nowPosition6 + number;
  moveToBalanceLevel_6(nowPosition6);
}

void moveToBalanceLevel_1(float bal)
{
  Serial.println(bal);
  targetCount = bal * encoderValue;
  integral = 0; //reset integral in PID control
  motorActive = true;
}
void moveToBalanceLevel_2(float bal)
{
  Serial.println(bal);
  targetCount2 = bal * encoderValue;
  integral2 = 0; //reset integral in PID control
  motor2Active = true;
}
void moveToBalanceLevel_3(float bal)
{
  Serial.println(bal);
  targetCount3 = bal * encoderValue;
  integral3 = 0; //reset integral in PID control
  motor3Active = true;
}
void moveToBalanceLevel_4(float bal)
{
  Serial.println(bal);
  targetCount4 = bal * encoderValue;
  integral4 = 0; //reset integral in PID control
  motor4Active = true;
}
void moveToBalanceLevel_5(float bal)
{
  Serial.println(bal);
  targetCount5 = bal * encoderValue;
  integral5 = 0; //reset integral in PID control
  motor3Active = true;
}
void moveToBalanceLevel_6(float bal)
{
  Serial.println(bal);
  targetCount6 = bal * encoderValue;
  integral6 = 0; //reset integral in PID control
  motor6Active = true;
}

void getVoltageFeedFromPID()
{
  int error;
  float I, D;

  error = targetCount - newPosition1;
  // Integral
  if (abs(error) < KiActivationErrorRange) {
    integral += error * usPassed;
    if (abs(error) < KiRange2) {
      I = Ki * KiRange2Factor * integral;
    } else {
      I = Ki * integral;
    }
  } else {
    integral = 0;
    I = 0;
  }
  //Derivative
  if (abs(error) < KdActivationErrorRange) {
    D = Kd * encoderSpeed;
  } else {
    D = 0;
  }

  //Derive driving voltage
  if (abs(targetCount - newPosition1) > activationCountError) {
    motorActive = true;
  }
  if (abs(targetCount - newPosition1) > acceptableCountError && motorActive) { // after motor has reached acceptableCountError, the error needs to excceed activationCountError to start the motor again.
    voltageFeed = Kp * (error) + I + D;

    if (voltageFeed > 0) {
      if (voltageFeed > maximalVoltage) {
        voltageFeed = maximalVoltage;
      }
      //motorDrive(motor1, turnDirection, voltageFeed);
    } else {
      if (voltageFeed < -maximalVoltage) {
        voltageFeed = -maximalVoltage;
      }
      //motorDrive(motor1, !turnDirection, -voltageFeed);
    }
  } else {
    integral = 0;
    voltageFeed = 0;
    
  }

  /////////////////
  int error2;
  float I2, D2;

  error2 = targetCount2 - newPosition2;
  // Integral
  if (abs(error2) < KiActivationErrorRange) {
    integral2 += error2 * usPassed2;
    if (abs(error2) < KiRange2) {
      I2 = Ki * KiRange2Factor * integral2;
    } else {
      I2 = Ki * integral2;
    }
  } else {
    integral2 = 0;
    I2 = 0;
  }
  //Derivative
  if (abs(error2) < KdActivationErrorRange) {
    D2 = Kd * encoderSpeed2;
  } else {
    D2 = 0;
  }

  //Derive driving voltage
  if (abs(targetCount2 - newPosition2) > activationCountError) {
    motor2Active = true;
  }
  if (abs(targetCount2 - newPosition2) > acceptableCountError && motor2Active) { // after motor has reached acceptableCountError, the error needs to excceed activationCountError to start the motor again.
    voltageFeed2 = Kp * (error2) + I2 + D2;
    
    if (voltageFeed2 > 0) {
      if (voltageFeed2 > maximalVoltage) {
        voltageFeed2 = maximalVoltage;
      }
      //motorDrive(motor1, turnDirection, voltageFeed);
    } else {
      if (voltageFeed2 < -maximalVoltage) {
        voltageFeed2 = -maximalVoltage;
      }
      //motorDrive(motor1, !turnDirection, -voltageFeed);
    }
  } else {
    integral2 = 0;
    voltageFeed2 = 0;
  }

  /////////////////
  int error3;
  float I3, D3;

  error3 = targetCount3 - newPosition3;
  // Integral
  if (abs(error3) < KiActivationErrorRange) {
    integral3 += error3 * usPassed3;
    if (abs(error3) < KiRange2) {
      I3 = Ki * KiRange2Factor * integral3;
    } else {
      I3 = Ki * integral3;
    }
  } else {
    integral3 = 0;
    I3 = 0;
  }
  //Derivative
  if (abs(error3) < KdActivationErrorRange) {
    D3 = Kd * encoderSpeed3;
  } else {
    D3 = 0;
  }

  //Derive driving voltage
  if (abs(targetCount3 - newPosition3) > activationCountError) {
    motor3Active = true;
  }
  if (abs(targetCount3 - newPosition3) > acceptableCountError && motor3Active) { // after motor has reached acceptableCountError, the error needs to excceed activationCountError to start the motor again.
    voltageFeed3 = Kp * (error3) + I3 + D3;

    if (voltageFeed3 > 0) {
      if (voltageFeed3 > maximalVoltage) {
        voltageFeed3 = maximalVoltage;
      }
      //motorDrive(motor1, turnDirection, voltageFeed);
    } else {
      if (voltageFeed3 < -maximalVoltage) {
        voltageFeed3 = -maximalVoltage;
      }
      //motorDrive(motor1, !turnDirection, -voltageFeed);
    }
  } else {
    integral3 = 0;
    voltageFeed3 = 0;
  }

  /////////////////
  int error4;
  float I4, D4;

  error4 = targetCount4 - newPosition4;
  // Integral
  if (abs(error4) < KiActivationErrorRange) {
    integral4 += error4 * usPassed4;
    if (abs(error4) < KiRange2) {
      I4 = Ki * KiRange2Factor * integral4;
    } else {
      I4 = Ki * integral4;
    }
  } else {
    integral4 = 0;
    I4 = 0;
  }
  //Derivative
  if (abs(error4) < KdActivationErrorRange) {
    D4 = Kd * encoderSpeed4;
  } else {
    D4 = 0;
  }

  //Derive driving voltage
  if (abs(targetCount4 - newPosition4) > activationCountError) {
    motor4Active = true;
  }
  if (abs(targetCount4 - newPosition4) > acceptableCountError && motor4Active) { // after motor has reached acceptableCountError, the error needs to excceed activationCountError to start the motor again.
    voltageFeed4 = Kp * (error4) + I4 + D4;

    if (voltageFeed4 > 0) {
      if (voltageFeed4 > maximalVoltage) {
        voltageFeed4 = maximalVoltage;
      }
    } else {
      if (voltageFeed4 < -maximalVoltage) {
        voltageFeed4 = -maximalVoltage;
      }
    }
  } else {
    integral4 = 0;
    voltageFeed4 = 0;
  }

  /////////////////
  int error5;
  float I5, D5;

  error5 = targetCount5 - newPosition5;
  // Integral
  if (abs(error5) < KiActivationErrorRange) {
    integral5 += error5 * usPassed5;
    if (abs(error5) < KiRange2) {
      I5 = Ki * KiRange2Factor * integral5;
    } else {
      I5 = Ki * integral5;
    }
  } else {
    integral5 = 0;
    I5 = 0;
  }
  //Derivative
  if (abs(error5) < KdActivationErrorRange) {
    D5 = Kd * encoderSpeed5;
  } else {
    D5 = 0;
  }

  //Derive driving voltage
  if (abs(targetCount5 - newPosition5) > activationCountError) {
    motor5Active = true;
  }
  if (abs(targetCount5 - newPosition5) > acceptableCountError && motor5Active) { // after motor has reached acceptableCountError, the error needs to excceed activationCountError to start the motor again.
    voltageFeed5 = Kp * (error5) + I5 + D5;

    if (voltageFeed5 > 0) {
      if (voltageFeed5 > maximalVoltage) {
        voltageFeed5 = maximalVoltage;
      }
    } else {
      if (voltageFeed5 < -maximalVoltage) {
        voltageFeed5 = -maximalVoltage;
      }
    }
  } else {
    integral5 = 0;
    voltageFeed5 = 0;
  }

  /////////////////
  int error6;
  float I6, D6;

  error6 = targetCount6 - newPosition6;
  // Integral
  if (abs(error6) < KiActivationErrorRange) {
    integral6 += error6 * usPassed6;
    if (abs(error6) < KiRange2) {
      I6 = Ki * KiRange2Factor * integral6;
    } else {
      I6 = Ki * integral6;
    }
  } else {
    integral6 = 0;
    I6 = 0;
  }
  //Derivative
  if (abs(error6) < KdActivationErrorRange) {
    D6 = Kd * encoderSpeed6;
  } else {
    D6 = 0;
  }

  //Derive driving voltage
  if (abs(targetCount6 - newPosition6) > activationCountError) {
    motor6Active = true;
  }
  if (abs(targetCount6 - newPosition6) > acceptableCountError && motor6Active) { // after motor has reached acceptableCountError, the error needs to excceed activationCountError to start the motor again.
    voltageFeed6 = Kp * (error6) + I6 + D6;

    if (voltageFeed6 > 0) {
      if (voltageFeed6 > maximalVoltage) {
        voltageFeed6 = maximalVoltage;
      }
    } else {
      if (voltageFeed6 < -maximalVoltage) {
        voltageFeed6 = -maximalVoltage;
      }
    }
  } else {
    integral6 = 0;
    voltageFeed6 = 0;
  }
}

void updateMotor() {
  //Serial.println("updateMotor");
  if (voltageFeed > 0) {
    motorDrive(motor1, turnDirection, voltageFeed);
  } else if (voltageFeed < 0) {
    motorDrive(motor1, !turnDirection, -voltageFeed);
  }
  else  {
    motorActive = false;
    motorStop(motor1);
    if(motorActive == false && motor2Active == false)
      motorsStandby();   
  }
}

void updateMotor2() {
  if (voltageFeed2 > 0) {
    motorDrive(motor2, turnDirection2, voltageFeed2);
  } else if (voltageFeed2 < 0) {
    motorDrive(motor2, !turnDirection2, -voltageFeed2);
  }
  else  {
    motor2Active = false;
    motorStop(motor2);
    if(motorActive == false && motor2Active == false)
      motorsStandby();
  }
}


void updateMotor3() {
  if (voltageFeed3 > 0) {
    motorDrive(motor3, turnDirection3, voltageFeed3);
  } else if (voltageFeed3 < 0) {
    motorDrive(motor3, !turnDirection3, -voltageFeed3);
  }
  else  {
    motor3Active = false;
    motorStop(motor3);
    if(motor3Active == false && motor4Active == false)
      motorsStandby2();
  }
}

void updateMotor4() {
  if (voltageFeed4 > 0) {
    //Serial.println(motor3);
    motorDrive(motor4, turnDirection4, voltageFeed4);
  } else if (voltageFeed4 < 0) {
    motorDrive(motor4, !turnDirection4, -voltageFeed4);
  }
  else  {
    motor4Active = false;
    motorStop(motor4);
    if(motor3Active == false && motor4Active == false)
      motorsStandby2();
  }
}

void updateMotor5() {
  if (voltageFeed5 > 0) {
    //Serial.println(motor3);
    motorDrive(motor5, turnDirection5, voltageFeed5);
  } else if (voltageFeed5 < 0) {
    motorDrive(motor5, !turnDirection5, -voltageFeed5);
  }
  else  {
    motor5Active = false;
    motorStop(motor5);
    if(motor5Active == false && motor6Active == false)
      motorsStandby3();
  }
}

void updateMotor6() {
  if (voltageFeed6 > 0) {
    //Serial.println(motor3);
    motorDrive(motor6, turnDirection6, voltageFeed6);
  } else if (voltageFeed6 < 0) {
    motorDrive(motor6, !turnDirection6, -voltageFeed6);
  }
  else  {
    motor6Active = false;
    motorStop(motor6);
    if(motor5Active == false && motor6Active == false)
      motorsStandby3();
  }
}

void getusPassed() {
  //Serial.println("getusPassed");
  static unsigned long prevTime = 0;
  unsigned long curTime;
  static unsigned long prevTime2 = 0;
  unsigned long curTime2;
  static unsigned long prevTime3 = 0;
  unsigned long curTime3;
  static unsigned long prevTime4 = 0;
  unsigned long curTime4;
  static unsigned long prevTime5 = 0;
  unsigned long curTime5;
  static unsigned long prevTime6 = 0;
  unsigned long curTime6;

  curTime = micros();
  usPassed = curTime - prevTime;
  prevTime = curTime;

  curTime2 = micros();
  usPassed2 = curTime2 - prevTime2;
  prevTime2 = curTime2;

  curTime3 = micros();
  usPassed3 = curTime3 - prevTime3;
  prevTime3 = curTime3;

  curTime4 = micros();
  usPassed4 = curTime4 - prevTime4;
  prevTime4 = curTime4;

  curTime5 = micros();
  usPassed5 = curTime5 - prevTime5;
  prevTime5 = curTime5;

  curTime6 = micros();
  usPassed6 = curTime6 - prevTime6;
  prevTime6 = curTime6;
}

void getEncoderSpeed() {
  //Serial.println("getEncoderSpeed");
  static unsigned long usPassedBetweenEncoderReadings = 0;
  static int prevCount = 0;
  float newSpeed;
  static unsigned long usPassed2BetweenEncoderReadings = 0;
  static int prevCount2 = 0;
  float newSpeed2;
  static unsigned long usPassed3BetweenEncoderReadings = 0;
  static int prevCount3 = 0;
  float newSpeed3;
  static unsigned long usPassed4BetweenEncoderReadings = 0;
  static int prevCount4 = 0;
  float newSpeed4;
  static unsigned long usPassed5BetweenEncoderReadings = 0;
  static int prevCount5 = 0;
  float newSpeed5;
  static unsigned long usPassed6BetweenEncoderReadings = 0;
  static int prevCount6 = 0;
  float newSpeed6;

  usPassedBetweenEncoderReadings += usPassed;
  if (usPassed > 1000) {
    newSpeed = (float)(curCount - prevCount) * 0.001;
    prevCount = curCount;
    usPassedBetweenEncoderReadings -= 1000;
    encoderSpeed = encoderSpeed * 0.8 + (newSpeed) * 0.2;
  }

  usPassed2BetweenEncoderReadings += usPassed2;
  if (usPassed2 > 1000) {
    newSpeed2 = (float)(curCount2 - prevCount2) * 0.001;
    prevCount2 = curCount2;
    usPassed2BetweenEncoderReadings -= 1000;
    encoderSpeed2 = encoderSpeed2 * 0.8 + (newSpeed2) * 0.2;
  }

  usPassed3BetweenEncoderReadings += usPassed3;
  if (usPassed3 > 1000) {
    newSpeed3 = (float)(curCount3 - prevCount3) * 0.001;
    prevCount3 = curCount3;
    usPassed3BetweenEncoderReadings -= 1000;
    encoderSpeed3 = encoderSpeed3 * 0.8 + (newSpeed3) * 0.2;
  }

  usPassed4BetweenEncoderReadings += usPassed4;
  if (usPassed4 > 1000) {
    newSpeed4 = (float)(curCount4 - prevCount4) * 0.001;
    prevCount4 = curCount4;
    usPassed4BetweenEncoderReadings -= 1000;
    encoderSpeed4 = encoderSpeed4 * 0.8 + (newSpeed4) * 0.2;
  }

  usPassed5BetweenEncoderReadings += usPassed5;
  if (usPassed5 > 1000) {
    newSpeed5 = (float)(curCount5 - prevCount5) * 0.001;
    prevCount5 = curCount5;
    usPassed5BetweenEncoderReadings -= 1000;
    encoderSpeed5 = encoderSpeed5 * 0.8 + (newSpeed5) * 0.2;
  }

  usPassed6BetweenEncoderReadings += usPassed6;
  if (usPassed6 > 1000) {
    newSpeed6 = (float)(curCount6 - prevCount6) * 0.001;
    prevCount6 = curCount6;
    usPassed6BetweenEncoderReadings -= 1000;
    encoderSpeed6 = encoderSpeed6 * 0.8 + (newSpeed6) * 0.2;
  }
}

void readEncoder()
{
  newPosition1 = Enc1.read();
  if (newPosition1 != oldPosition1) {
    oldPosition1 = newPosition1;
//    Serial.println(newPosition1);
  }
  
  newPosition2 = Enc2.read();
  if (newPosition2 != oldPosition2) {
    oldPosition2 = newPosition2;
//    Serial.println(newPosition2);
  }

  newPosition3 = Enc3.read();
  if (newPosition3 != oldPosition3) {
    oldPosition3 = newPosition3;
//    Serial.println(newPosition3);
  }

  newPosition4 = Enc4.read();
  if (newPosition4 != oldPosition4) {
    oldPosition4 = newPosition4;
//    Serial.println(newPosition4);
  }
  
  newPosition5 = Enc5.read();
  if (newPosition5 != oldPosition5) {
    oldPosition5 = newPosition5;
//    Serial.println(newPosition5);
  }

  newPosition6 = Enc6.read();
  if (newPosition6 != oldPosition6) {
    oldPosition6 = newPosition6;
//    Serial.println(newPosition6);
  }
}





void motorDrive(int motorNumber, bool motorDirection, unsigned short motorVoltage)
{
  /*
    This Drives a specified motor, in a specific direction, at a specified Voltage:
    - motorNumber: motor1 or motor2 ---> Motor 1 or Motor 2
    - motorDirection: turnCW or turnCCW ---> clockwise or counter-clockwise
    - motorVoltage: 0 to 255 ---> 0 = stop / 255 = fast
  */

  bool pinIn1;  //Relates to AIN1 or BIN1 (depending on the motor number specified)

  //Specify the Direction to turn the motor
  //Clockwise: AIN1/BIN1 = HIGH and AIN2/BIN2 = LOW
  //Counter-Clockwise: AIN1/BIN1 = LOW and AIN2/BIN2 = HIGH
  if (motorDirection == true)
    pinIn1 = HIGH;
  else
    pinIn1 = LOW;

  //Select the motor to turn, and set the direction and the Voltage
  if (motorNumber == motor1)
  {
//    Serial.println("motor1 run");
    digitalWrite(M1_pinAIN1, pinIn1);
    digitalWrite(M1_pinAIN2, !pinIn1);  //This is the opposite of the AIN1
    analogWrite(M1_pinPWMA, motorVoltage);

    //Finally , make sure STBY is disabled - pull it HIGH
    digitalWrite(pinSTBY, HIGH);
  }
  else if (motorNumber == motor2){
//    Serial.println("motor2 run");
    digitalWrite(M2_pinBIN1, !pinIn1);
    digitalWrite(M2_pinBIN2, pinIn1);  //This is the opposite of the BIN1
    analogWrite(M2_pinPWMB, motorVoltage);

    //Serial.println(M2_pinPWMB);
    //Finally , make sure STBY is disabled - pull it HIGH
    digitalWrite(pinSTBY, HIGH);
  }
  else if(motorNumber == motor3){
//    Serial.println("motor3 run");
    digitalWrite(M3_pinAIN1, !pinIn1);
    digitalWrite(M3_pinAIN2, pinIn1);  //This is the opposite of the BIN1
    analogWrite(M3_pinPWMA, motorVoltage);
    
    //Finally , make sure STBY is disabled - pull it HIGH
    digitalWrite(pinSTBY_2, HIGH);
  }
  else if(motorNumber == motor4){
//    Serial.println("motor4 run");
    digitalWrite(M4_pinBIN1, !pinIn1);
    digitalWrite(M4_pinBIN2, pinIn1);  //This is the opposite of the BIN1
    analogWrite(M4_pinPWMB, motorVoltage);
    
    //Finally , make sure STBY is disabled - pull it HIGH
    digitalWrite(pinSTBY_2, HIGH);
  }
  else if(motorNumber == motor5){
//    Serial.println("motor5 run");
    digitalWrite(M5_pinAIN1, !pinIn1);
    digitalWrite(M5_pinAIN2, pinIn1);  //This is the opposite of the BIN1
    analogWrite(M5_pinPWMA, motorVoltage);
    
    //Finally , make sure STBY is disabled - pull it HIGH
    digitalWrite(pinSTBY_3, HIGH);
  }
  else if(motorNumber == motor6){
//    Serial.println("motor6 run");
    digitalWrite(M6_pinBIN1, !pinIn1);
    digitalWrite(M6_pinBIN2, pinIn1);  //This is the opposite of the BIN1
    analogWrite(M6_pinPWMB, motorVoltage);
    
    //Finally , make sure STBY is disabled - pull it HIGH
    digitalWrite(pinSTBY_3, HIGH);
  }

}

void motorBrake(bool motorNumber)
{
  /*
    This "Short Brake"s the specified motor, by setting Voltage to zero
  */

  if (motorNumber == motor1)
    analogWrite(M1_pinPWMA, 0);
  else if(motorNumber == motor2)
    analogWrite(M2_pinPWMB, 0);
  else if(motorNumber == motor3)
    analogWrite(M3_pinPWMA, 0);
  else if(motorNumber == motor4)
    analogWrite(M4_pinPWMB, 0);
  else if(motorNumber == motor5)
    analogWrite(M5_pinPWMA, 0);
  else if(motorNumber == motor6)
    analogWrite(M6_pinPWMB, 0);
}


void motorStop(bool motorNumber)
{
  /*
    This stops the specified motor by setting both IN pins to LOW
  */
  if (motorNumber == motor1) {
    digitalWrite(M1_pinAIN1, LOW);
    digitalWrite(M1_pinAIN2, LOW);
  }
  else if(motorNumber == motor2)
  {
    digitalWrite(M2_pinBIN1, LOW);
    digitalWrite(M2_pinBIN2, LOW);
  }
  else if(motorNumber == motor3){
    digitalWrite(M3_pinAIN1, LOW);
    digitalWrite(M3_pinAIN2, LOW);
  }
  else if(motorNumber == motor4){
    digitalWrite(M4_pinBIN1, LOW);
    digitalWrite(M4_pinBIN2, LOW);
  }
  else if(motorNumber == motor5){
    digitalWrite(M5_pinAIN1, LOW);
    digitalWrite(M5_pinAIN2, LOW);
  }
  else if(motorNumber == motor6){
    digitalWrite(M6_pinBIN1, LOW);
    digitalWrite(M6_pinBIN2, LOW);
  }
}

/*This puts the motors into Standby Mode*/
void motorsStandby()
{
  digitalWrite(pinSTBY, LOW);
}
void motorsStandby2()
{
  digitalWrite(pinSTBY_2, LOW);
}
void motorsStandby3()
{
  digitalWrite(pinSTBY_3, LOW);
}
